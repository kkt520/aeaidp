package com.agileai.hotweb.controller;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class LoginHandler  extends BaseHandler{
	public LoginHandler(){
		super();
	}
	
	@PageAction
	public ViewRenderer ensureLogin(DataParam param){
		String  responseText = FAIL; 
		User user =  (User) this.getUser();
//		System.out.println("user is " + user);
		if(user != null){
			responseText = SUCCESS;
		}else{
			responseText = FAIL;
		}
		return new AjaxRenderer(responseText);
	}
}
