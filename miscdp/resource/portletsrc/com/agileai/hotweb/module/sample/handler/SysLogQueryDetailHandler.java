package com.agileai.hotweb.module.sample.handler;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.QueryModelDetailHandler;
import com.agileai.hotweb.module.sample.service.SysLogQuery;

public class SysLogQueryDetailHandler
        extends QueryModelDetailHandler {
    public SysLogQueryDetailHandler() {
        super();
        this.serviceId = buildServiceId(SysLogQuery.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected SysLogQuery getService() {
        return (SysLogQuery) this.lookupService(this.getServiceId());
    }
}
