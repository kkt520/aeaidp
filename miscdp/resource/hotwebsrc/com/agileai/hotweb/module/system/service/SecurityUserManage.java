package com.agileai.hotweb.module.system.service;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;

public interface SecurityUserManage extends 
      TreeAndContentManage {
	public DataRow getCopyContentRecord (String grpId,String userId);
	public DataRow getMoveContentRecord (String grpId,String userId);
	public void deleteSecurityUserRecord(String userId);
	public void deleteSecurityUserRelation(String grpId,String userId);
	public List<DataRow> findRoleTreeRecords(DataParam param);
	public List<DataRow> retrieveUserList(DataParam param);
	public List<DataRow> findSecurityUserRelation(String grpId,String userId);
	public List<DataRow> findUserActualRoleRecords(String rgId);
	public List<DataRow> findPickFillRecords(String rgId);
	public void insertUserActualRoleRecords(DataParam param);
	public void deletTureContentRecord(String urgId);
	public DataRow getUserActualRoleRelation(String rgId,String userId);
	
}
