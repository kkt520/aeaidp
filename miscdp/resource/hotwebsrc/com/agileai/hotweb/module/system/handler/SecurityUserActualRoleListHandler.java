package com.agileai.hotweb.module.system.handler;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataMap;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;
import com.agileai.hotweb.controller.core.TreeAndContentManageListHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.hotweb.module.system.service.SecurityUserManage;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;


public class SecurityUserActualRoleListHandler 
			extends TreeAndContentManageListHandler{
    public SecurityUserActualRoleListHandler() {
        super();
        this.serviceId = buildServiceId(SecurityUserManage.class);
        this.rootColumnId = "00000000-0000-0000-00000000000000000";
        this.defaultTabId = "Position";
        this.columnIdField = "GRP_ID";
        this.columnNameField = "GRP_NAME";
        this.columnParentIdField = "GRP_PID";
        this.columnSortField = "GRP_SORT";
    }

    protected void processPageAttributes(DataParam param) {
    	
    }

    protected void initParameters(DataParam param) {

    }
    
    public ViewRenderer prepareDisplay(DataParam param){
		initParameters(param);
		processPageAttributes(param);
		this.setAttributes(param);
		String columnId = param.get("columnId",this.rootColumnId);
		this.setAttribute("columnId", columnId);
		this.setAttribute("isRootColumnId",String.valueOf(this.rootColumnId.equals(columnId)));
		param.put("RG_ID",columnId);
		String tabId = param.get(TreeAndContentManage.TAB_ID,this.defaultTabId);
		
		this.setAttribute(TreeAndContentManage.TAB_ID, tabId);
		this.setAttribute(TreeAndContentManage.TAB_INDEX, getTabIndex(tabId));
		return new LocalRenderer(getPage());
	}
    
    @PageAction
	public ViewRenderer tableJson(DataParam param) {
    	String responseText = "";
    	try {
			String columnId = param.get("columnId", this.rootColumnId);
			param.put("columnId",columnId);
			String rgId = param.get("RG_ID");
			JSONObject jsonObject = new JSONObject();
			List<DataRow> arrayList = new ArrayList<DataRow>();
			DataMap userSex = FormSelectFactory.create("USER_SEX").getContent();
			DataMap sysValidType = FormSelectFactory.create("SYS_VALID_TYPE").getContent();
			SecurityUserManage securityUserManage = (SecurityUserManage)this.lookupService(SecurityUserManage.class);
			List<DataRow> rsList = securityUserManage.findUserActualRoleRecords(rgId);
			for (int i = 0; i < rsList.size(); i++) {
				DataRow dataRow = new DataRow();
				DataRow row = rsList.get(i);
				dataRow.put("ID", i+1);
				dataRow.put("USER_ID", row.get("USER_ID"));
				dataRow.put("USER_CODE", row.get("USER_CODE"));
				dataRow.put("USER_NAME", row.get("USER_NAME"));
				dataRow.put("USER_SEX", userSex.get(row.get("USER_SEX")));
				dataRow.put("USER_STATE", sysValidType.get(row.get("USER_STATE")));
				dataRow.put("GRP_ID", row.get("GRP_ID"));
				dataRow.put("USER_PHONE", row.get("USER_PHONE"));
				arrayList.add(dataRow);
			}
			jsonObject.put("arrayList", arrayList);
			JSONArray jsonArray = (JSONArray) jsonObject.get("arrayList");
			responseText = jsonArray.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
    	return new AjaxRenderer(responseText);
    }
    
    public ViewRenderer doQueryEmpAction(DataParam param){

    	 return new LocalRenderer(getPage());
	}
    
    protected TreeBuilder provideTreeBuilder(DataParam param) {
    	SecurityUserManage service = this.getService();
        List<DataRow> menuRecords = service.findTreeRecords(new DataParam());
        TreeBuilder treeBuilder = new TreeBuilder(menuRecords,
                                                  this.columnIdField,
                                                  this.columnNameField,
                                                  this.columnParentIdField);

        return treeBuilder;
    }
    
    public ViewRenderer doDeletePosempAction(DataParam param){
    	SecurityUserManage securityUserManage = (SecurityUserManage)this.lookupService(SecurityUserManage.class);
    	String userId = param.getString("USER_ID");
    	String rgId = param.getString("RG_ID");
    	DataRow row = securityUserManage.getUserActualRoleRelation(rgId,userId);
    	String urgId = row.getString("URG_ID");
    	securityUserManage.deletTureContentRecord(urgId);
		return prepareDisplay(param);
	}

    protected List<String> getTabList() {
        List<String> result = new ArrayList<String>();
        result.add(SecurityUserManage.BASE_TAB_ID);
        result.add("Position");
        return result;
    }

    protected SecurityUserManage getService() {
        return (SecurityUserManage) this.lookupService(this.getServiceId());
    }
}
