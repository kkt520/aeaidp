/*
Navicat Oracle Data Transfer
Oracle Client Version : 11.2.0.1.0

Source Server         : local_oracle
Source Server Version : 110200
Source Host           : :1521
Source Schema         : DEMO

Target Server Type    : ORACLE
Target Server Version : 110200
File Encoding         : 65001

Date: 2018-01-16 15:50:25
*/


-- ----------------------------
-- Table structure for SECURITY_GROUP
-- ----------------------------
DROP TABLE "SECURITY_GROUP";
CREATE TABLE "SECURITY_GROUP" (
"GRP_ID" NVARCHAR2(36) NOT NULL ,
"GRP_CODE" NVARCHAR2(32) NULL ,
"GRP_NAME" NVARCHAR2(32) NULL ,
"GRP_PID" NVARCHAR2(36) NULL ,
"GRP_DESC" NVARCHAR2(128) NULL ,
"GRP_STATE" NVARCHAR2(1) NULL ,
"GRP_SORT" NUMBER(11) NULL ,
"GRP_TYPE" NVARCHAR2(32) NULL ,
"GRP_RANK" NVARCHAR2(32) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SECURITY_GROUP
-- ----------------------------
INSERT INTO "SECURITY_GROUP" VALUES ('00000000-0000-0000-00000000000000000', 'Root', '公司集团', null, null, '1', '0', 'company', '1');
INSERT INTO "SECURITY_GROUP" VALUES ('19690068-D8B4-4B97-8E4D-5D837A8B9AD5', 'C0006', '行政部', 'EEDB6308-A8A9-40CF-A078-4606B6EF4622', null, '1', '5', 'department', '1');
INSERT INTO "SECURITY_GROUP" VALUES ('1C023241-9315-4764-A7F1-7E90537CD7A7', 'A0001', '技术部', '00000000-0000-0000-00000000000000000', null, '1', '3', 'department', '1');
INSERT INTO "SECURITY_GROUP" VALUES ('3A26DB2F-ECA1-4C89-A971-94F501EFCD41', 'D0001', '上海分公司', '00000000-0000-0000-00000000000000000', null, '1', '6', 'company', '1');
INSERT INTO "SECURITY_GROUP" VALUES ('6FC0686E-39D0-4DCE-AF54-A80738EAA28B', 'C0005', '销售部', 'EEDB6308-A8A9-40CF-A078-4606B6EF4622', null, '1', '4', 'department', '1');
INSERT INTO "SECURITY_GROUP" VALUES ('8BF2F65A-6765-46CC-A853-9B75304FC7FD', '002.01', '技术部', 'EEDB6308-A8A9-40CF-A078-4606B6EF4622', null, '1', '2', 'department', '1');
INSERT INTO "SECURITY_GROUP" VALUES ('B17F9D2B-837F-4247-8A34-19E71BFC6DB4', 'D0006', '销售部', '3A26DB2F-ECA1-4C89-A971-94F501EFCD41', null, '1', '4', 'department', '1');
INSERT INTO "SECURITY_GROUP" VALUES ('B6F11BD3-DE55-4F14-A400-540B9E4F45F3', 'A0002', '信息部', '00000000-0000-0000-00000000000000000', null, '1', '2', 'department', '1');
INSERT INTO "SECURITY_GROUP" VALUES ('DFA63AC0-6D21-443F-B26C-97118A1808B8', 'D0002', '技术部', '3A26DB2F-ECA1-4C89-A971-94F501EFCD41', null, '1', '1', 'department', '2');
INSERT INTO "SECURITY_GROUP" VALUES ('E068CD3C-E676-4601-B5B8-5CB9F04C5D37', 'C0002', '信息部', 'EEDB6308-A8A9-40CF-A078-4606B6EF4622', null, '1', '3', 'department', '1');
INSERT INTO "SECURITY_GROUP" VALUES ('E13A2B44-8965-4023-9394-71CA9BF4B899', 'D0005', '信息部', '3A26DB2F-ECA1-4C89-A971-94F501EFCD41', null, '1', '3', 'department', '1');
INSERT INTO "SECURITY_GROUP" VALUES ('E49B519B-7DC3-4104-BEBF-CC0B05833A49', 'A0003', '行政部', '00000000-0000-0000-00000000000000000', null, '1', '4', 'department', '1');
INSERT INTO "SECURITY_GROUP" VALUES ('EEDB6308-A8A9-40CF-A078-4606B6EF4622', 'B0001', '北京分公司', '00000000-0000-0000-00000000000000000', null, '1', '5', 'company', '1');
INSERT INTO "SECURITY_GROUP" VALUES ('FE8A88A0-5CFB-42C7-B398-E4AE74A8E39D', 'D0003', '行政部', '3A26DB2F-ECA1-4C89-A971-94F501EFCD41', null, null, '2', 'department', '1');

-- ----------------------------
-- Table structure for SECURITY_GROUP_AUTH
-- ----------------------------
DROP TABLE "SECURITY_GROUP_AUTH";
CREATE TABLE "SECURITY_GROUP_AUTH" (
"GRP_AUTH_ID" NVARCHAR2(36) NOT NULL ,
"GRP_ID" NVARCHAR2(36) NULL ,
"RES_TYPE" NVARCHAR2(32) NULL ,
"RES_ID" NVARCHAR2(36) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SECURITY_GROUP_AUTH
-- ----------------------------
INSERT INTO "SECURITY_GROUP_AUTH" VALUES ('4C20EDAC-0A0B-45BB-8B71-B557A94E7429', 'B6F11BD3-DE55-4F14-A400-540B9E4F45F3', 'Navigater', '02');

-- ----------------------------
-- Table structure for SECURITY_RG_AUTH
-- ----------------------------
DROP TABLE "SECURITY_RG_AUTH";
CREATE TABLE "SECURITY_RG_AUTH" (
"RG_AUTH_ID" NCHAR(36) NOT NULL ,
"RG_ID" NCHAR(36) NULL ,
"RES_TYPE" NVARCHAR2(32) NULL ,
"RES_ID" NCHAR(36) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SECURITY_RG_AUTH
-- ----------------------------
INSERT INTO "SECURITY_RG_AUTH" VALUES ('C922C243-8C3C-479B-9E84-792EDAEDEB43', 'A0CFD5D1-6D43-4A74-8A92-1B06F6AB95D3', 'Menu', 'E31A6BD9-8842-4F16-A2B2-929A2E831173');
INSERT INTO "SECURITY_RG_AUTH" VALUES ('DAC29F7B-3498-4227-940A-70F3E930C00B', 'A0CFD5D1-6D43-4A74-8A92-1B06F6AB95D3', 'Menu', '8F1E1FC2-423E-4216-BBEF-916DEE23DEDC');
INSERT INTO "SECURITY_RG_AUTH" VALUES ('E86369BD-6B18-4E62-947A-56C1C265F232', 'A0CFD5D1-6D43-4A74-8A92-1B06F6AB95D3', 'Menu', '00000000-0000-0000-00000000000000000');

-- ----------------------------
-- Table structure for SECURITY_ROLE
-- ----------------------------
DROP TABLE "SECURITY_ROLE";
CREATE TABLE "SECURITY_ROLE" (
"ROLE_ID" NVARCHAR2(36) NOT NULL ,
"ROLE_CODE" NVARCHAR2(32) NULL ,
"ROLE_NAME" NVARCHAR2(32) NULL ,
"ROLE_TYPE" NVARCHAR2(32) NULL ,
"ROLE_PID" NVARCHAR2(36) NULL ,
"ROLE_DESC" NVARCHAR2(128) NULL ,
"ROLE_STATE" NVARCHAR2(32) NULL ,
"ROLE_SORT" NUMBER(11) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SECURITY_ROLE
-- ----------------------------
INSERT INTO "SECURITY_ROLE" VALUES ('00000000-0000-0000-00000000000000000', 'System', '系统角色', 'menu', null, null, '1', null);
INSERT INTO "SECURITY_ROLE" VALUES ('8752C789-44BD-4941-AEF1-B9CC8B5C3CC6', 'ITManager', 'IT主管', 'part', '00000000-0000-0000-00000000000000000', null, '1', '3');

-- ----------------------------
-- Table structure for SECURITY_ROLE_AUTH
-- ----------------------------
DROP TABLE "SECURITY_ROLE_AUTH";
CREATE TABLE "SECURITY_ROLE_AUTH" (
"ROLE_AUTH_ID" NVARCHAR2(36) NOT NULL ,
"ROLE_ID" NVARCHAR2(36) NULL ,
"RES_TYPE" NVARCHAR2(32) NULL ,
"RES_ID" NVARCHAR2(36) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SECURITY_ROLE_AUTH
-- ----------------------------
INSERT INTO "SECURITY_ROLE_AUTH" VALUES ('586BF6FA-4066-4C65-9352-C0A17B6E2170', '8752C789-44BD-4941-AEF1-B9CC8B5C3CC6', 'Menu', '00000000-0000-0000-00000000000000000');

-- ----------------------------
-- Table structure for SECURITY_ROLE_GROUP_REL
-- ----------------------------
DROP TABLE "SECURITY_ROLE_GROUP_REL";
CREATE TABLE "SECURITY_ROLE_GROUP_REL" (
"RG_ID" NCHAR(36) NOT NULL ,
"GRP_ID" NVARCHAR2(36) NOT NULL ,
"ROLE_ID" NVARCHAR2(36) NOT NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SECURITY_ROLE_GROUP_REL
-- ----------------------------
INSERT INTO "SECURITY_ROLE_GROUP_REL" VALUES ('A0CFD5D1-6D43-4A74-8A92-1B06F6AB95D3', '00000000-0000-0000-00000000000000000', '8752C789-44BD-4941-AEF1-B9CC8B5C3CC6');
INSERT INTO "SECURITY_ROLE_GROUP_REL" VALUES ('F49AD3BA-A714-44E7-9B97-4B4F595C20F1', 'B6F11BD3-DE55-4F14-A400-540B9E4F45F3', '8752C789-44BD-4941-AEF1-B9CC8B5C3CC6');

-- ----------------------------
-- Table structure for SECURITY_USER
-- ----------------------------
DROP TABLE "SECURITY_USER";
CREATE TABLE "SECURITY_USER" (
"USER_ID" NVARCHAR2(36) NOT NULL ,
"USER_CODE" NVARCHAR2(32) NULL ,
"USER_NAME" NVARCHAR2(32) NULL ,
"USER_PWD" NVARCHAR2(32) NULL ,
"USER_SEX" NVARCHAR2(1) NULL ,
"USER_DESC" NVARCHAR2(128) NULL ,
"USER_STATE" NVARCHAR2(32) NULL ,
"USER_SORT" NUMBER(11) NULL ,
"USER_MAIL" NVARCHAR2(64) NULL ,
"USER_PHONE" NVARCHAR2(64) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SECURITY_USER
-- ----------------------------
INSERT INTO "SECURITY_USER" VALUES ('4820B253-DF9A-476F-B1B1-1D21754256CF', 'user', '默认用户', '4124BC0A9335C27F086F24BA207A4912', 'M', '默认内置用户，勿删！！', '1', '2', null, null);
INSERT INTO "SECURITY_USER" VALUES ('7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', 'admin', '管理员', '21232F297A57A5A743894A0E4A801FC3', 'M', '内置账户，勿删！！', '1', '1', null, null);

-- ----------------------------
-- Table structure for SECURITY_USER_AUTH
-- ----------------------------
DROP TABLE "SECURITY_USER_AUTH";
CREATE TABLE "SECURITY_USER_AUTH" (
"USER_AUTH_ID" NVARCHAR2(36) NOT NULL ,
"USER_ID" NVARCHAR2(36) NULL ,
"RES_TYPE" NVARCHAR2(32) NULL ,
"RES_ID" NVARCHAR2(36) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SECURITY_USER_AUTH
-- ----------------------------
INSERT INTO "SECURITY_USER_AUTH" VALUES ('1279D0FE-B366-4D7E-9C99-B6B3424387F3', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO "SECURITY_USER_AUTH" VALUES ('4B523CC8-775A-49A1-BF87-28A7FD05B81F', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', 'Menu', '271576B9-3F0F-49B6-8505-3999FDB3E795');
INSERT INTO "SECURITY_USER_AUTH" VALUES ('5194F17D-B001-44EE-9FAB-21F836CB49A8', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC', 'Handler', '494DF09B-7573-4CCA-85C1-97F4DC58C86B');
INSERT INTO "SECURITY_USER_AUTH" VALUES ('63253005-BECA-4C51-A764-C36342DA009F', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC', 'Menu', '00000000-0000-0000-00000000000000001');
INSERT INTO "SECURITY_USER_AUTH" VALUES ('7CDFBA30-A40B-4958-820F-37171A5782C5', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', 'Menu', '11CB05AE-204D-4207-9AF6-ABB7084F0C25');
INSERT INTO "SECURITY_USER_AUTH" VALUES ('81882EE9-6E6C-4BEF-9F41-55D135784C0C', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', 'Menu', 'D3FAC05D-12F3-4576-ABC6-DAD037D1F4F9');
INSERT INTO "SECURITY_USER_AUTH" VALUES ('97F07CAD-F046-48FB-B03B-5A0AA049FBD5', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC', 'Menu', '67BA273A-DD31-48D0-B78C-1D60D5316074');
INSERT INTO "SECURITY_USER_AUTH" VALUES ('CB82AB8B-2066-4647-B8B3-2E2BFC0394CF', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', 'Menu', '590EE84E-5EC9-4197-A37C-CE67E191DC56');
INSERT INTO "SECURITY_USER_AUTH" VALUES ('D76FA80F-51AB-4A5B-9594-6DAE53A3C1E3', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC', 'Operation', '3EC41FB8-ED98-41F3-B86D-F7473AEDD002');
INSERT INTO "SECURITY_USER_AUTH" VALUES ('E3BB835C-FE79-4B8B-B412-91A0B7D92CCA', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', 'Menu', '00000000-0000-0000-00000000000000000');
INSERT INTO "SECURITY_USER_AUTH" VALUES ('ECCA6C17-0077-4FDA-94A7-D4637AD5D8AC', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24', 'Menu', 'B7836820-C2E2-4A87-8497-A1B22A0EA9E0');

-- ----------------------------
-- Table structure for SECURITY_USER_GROUP_REL
-- ----------------------------
DROP TABLE "SECURITY_USER_GROUP_REL";
CREATE TABLE "SECURITY_USER_GROUP_REL" (
"GU_ID" NCHAR(36) NOT NULL ,
"GRP_ID" NVARCHAR2(36) NULL ,
"USER_ID" NVARCHAR2(36) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SECURITY_USER_GROUP_REL
-- ----------------------------
INSERT INTO "SECURITY_USER_GROUP_REL" VALUES ('0665161F-029D-4329-9EE7-3F035DC4CB34', '00000000-0000-0000-00000000000000000', '7DE6ED51-3F4B-4BE6-84A6-17BC6186CC24');
INSERT INTO "SECURITY_USER_GROUP_REL" VALUES ('096126DD-39AB-44CF-A09E-671AD00BF5F8', 'B6F11BD3-DE55-4F14-A400-540B9E4F45F3', '9A0A9DE7-608A-4B2B-B1C7-F6C11FD1A94E');
INSERT INTO "SECURITY_USER_GROUP_REL" VALUES ('09DF8B4D-FC87-48F0-B799-EEDA383DB808', 'B6F11BD3-DE55-4F14-A400-540B9E4F45F3', 'B64DCBC3-F28A-4DC8-92F1-1FB10367CACC');

-- ----------------------------
-- Table structure for SECURITY_USER_RG_REL
-- ----------------------------
DROP TABLE "SECURITY_USER_RG_REL";
CREATE TABLE "SECURITY_USER_RG_REL" (
"URG_ID" NVARCHAR2(36) NOT NULL ,
"USER_ID" NVARCHAR2(36) NULL ,
"RG_ID" NVARCHAR2(36) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SECURITY_USER_RG_REL
-- ----------------------------

-- ----------------------------
-- Table structure for SYS_CODELIST
-- ----------------------------
DROP TABLE "SYS_CODELIST";
CREATE TABLE "SYS_CODELIST" (
"TYPE_ID" NVARCHAR2(32) NOT NULL ,
"CODE_ID" NVARCHAR2(64) NOT NULL ,
"CODE_NAME" NVARCHAR2(64) NULL ,
"CODE_DESC" NVARCHAR2(512) NULL ,
"CODE_SORT" NUMBER(11) NULL ,
"CODE_FLAG" NVARCHAR2(32) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SYS_CODELIST
-- ----------------------------
INSERT INTO "SYS_CODELIST" VALUES ('AuthedHandlerId', 'Bottom', 'BottomHandler', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('AuthedHandlerId', 'Building', 'BuildingHandler', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('AuthedHandlerId', 'Homepage', 'HomepageHandler', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('AuthedHandlerId', 'Logo', 'LogoHandler', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('AuthedHandlerId', 'MainWin', 'MainWinHandler', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('AuthedHandlerId', 'MenuTree', 'MenuTreeHandler', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('AuthedHandlerId', 'Navigater', 'NavigaterHandler', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('BOOL_DEFINE', 'N', '否', null, '2', '1');
INSERT INTO "SYS_CODELIST" VALUES ('BOOL_DEFINE', 'Y', '是', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('CODE_TYPE_GROUP', 'app_code_define', '应用编码', 'null', '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('CODE_TYPE_GROUP', 'sys_code_define', '系统编码', '系统编码123a1', '3', '1');
INSERT INTO "SYS_CODELIST" VALUES ('FUNCTION_TYPE', 'funcmenu', '功能菜单', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('FUNCTION_TYPE', 'funcnode', '功能节点', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('HANDLER_TYPE', 'MAIN', '主控制器', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('HANDLER_TYPE', 'OTHER', '其他控制器', null, '2', '1');
INSERT INTO "SYS_CODELIST" VALUES ('INFO_TYPE', 'menu', '目录', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('INFO_TYPE', 'part', '角色', null, '2', '1');
INSERT INTO "SYS_CODELIST" VALUES ('MENUTREE_CASCADE', '0', '关闭', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('MENUTREE_CASCADE', '1', '展开', null, '2', '1');
INSERT INTO "SYS_CODELIST" VALUES ('OPER_CTR_TYPE', 'disableMode', '不能操作', null, '2', '1');
INSERT INTO "SYS_CODELIST" VALUES ('OPER_CTR_TYPE', 'hiddenMode', '隐藏按钮', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('ORGNAZITION_RANK', '1', '一级', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('ORGNAZITION_RANK', '2', '二级', null, '2', '1');
INSERT INTO "SYS_CODELIST" VALUES ('ORGNAZITION_RANK', '3', '三级', null, '3', '1');
INSERT INTO "SYS_CODELIST" VALUES ('ORGNAZITION_TYPE', 'company', '公司', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('ORGNAZITION_TYPE', 'department', '部门', null, '2', '1');
INSERT INTO "SYS_CODELIST" VALUES ('POSITION_TYPE', 'dummy_postion', '虚拟岗位', 'null', '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('POSITION_TYPE', 'real_postion', '实际岗位', 'null', '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('RES_TYPE', 'IMAGE', '图片文件', null, '2', '1');
INSERT INTO "SYS_CODELIST" VALUES ('RES_TYPE', 'ISO', '镜像文件', null, '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('RES_TYPE', 'VIDEO', '视频文件', null, '3', '1');
INSERT INTO "SYS_CODELIST" VALUES ('SYS_VALID_TYPE', '0', '无效', 'null', '2', '1');
INSERT INTO "SYS_CODELIST" VALUES ('SYS_VALID_TYPE', '1', '有效', 'null', '1', '1');
INSERT INTO "SYS_CODELIST" VALUES ('UNIT_TYPE', 'dept', '部门', null, '10', '1');
INSERT INTO "SYS_CODELIST" VALUES ('UNIT_TYPE', 'org', '机构', null, '20', '1');
INSERT INTO "SYS_CODELIST" VALUES ('UNIT_TYPE', 'post', '岗位', null, '30', '1');
INSERT INTO "SYS_CODELIST" VALUES ('USER_SEX', 'F', '女', null, '2', '1');
INSERT INTO "SYS_CODELIST" VALUES ('USER_SEX', 'M', '男', 'null', '1', '1');

-- ----------------------------
-- Table structure for SYS_CODETYPE
-- ----------------------------
DROP TABLE "SYS_CODETYPE";
CREATE TABLE "SYS_CODETYPE" (
"TYPE_ID" NVARCHAR2(32) NOT NULL ,
"TYPE_NAME" NVARCHAR2(32) NULL ,
"TYPE_GROUP" NVARCHAR2(32) NULL ,
"TYPE_DESC" NVARCHAR2(128) NULL ,
"IS_CACHED" NCHAR(1) NULL ,
"IS_UNITEADMIN" NCHAR(1) NULL ,
"IS_EDITABLE" NCHAR(1) NULL ,
"LEGNTT_LIMIT" NVARCHAR2(6) NULL ,
"CHARACTER_LIMIT" NCHAR(1) NULL ,
"EXTEND_SQL" NCHAR(1) NULL ,
"SQL_BODY" NVARCHAR2(512) NULL ,
"SQL_COND" NVARCHAR2(256) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SYS_CODETYPE
-- ----------------------------
INSERT INTO "SYS_CODETYPE" VALUES ('AuthedHandlerId', '认证Handler定义', 'sys_code_define', null, 'Y', 'Y', 'Y', null, 'B', 'N', null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('BOOL_DEFINE', '布尔定义', 'sys_code_define', null, 'Y', 'Y', 'Y', '1', 'C', null, null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('CODE_TYPE_GROUP', '编码类型分组', 'app_code_define', '编码类型分组', null, null, null, null, null, null, null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('FUNCTION_TYPE', '功能类型', 'sys_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('HANDLER_TYPE', '控制器类型', 'sys_code_define', null, 'N', 'Y', 'Y', '32', 'C', null, null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('INFO_TYPE', '信息类别', 'app_code_define', null, 'Y', 'Y', 'Y', null, null, 'N', null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('MENUTREE_CASCADE', '是否展开', 'sys_code_define', null, 'Y', 'Y', 'Y', '1', 'N', 'N', null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('OPER_CTR_TYPE', '操作控制类型', 'sys_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('ORGNAZITION_RANK', '组织级别', 'app_code_define', null, 'Y', 'Y', 'Y', null, null, 'N', null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('ORGNAZITION_TYPE', '组织类别', 'app_code_define', null, 'Y', 'Y', 'Y', null, null, 'N', null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('POSITION_TYPE', '岗位类型', 'app_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('RES_TYPE', '资源类型', 'sys_code_define', null, 'N', 'Y', 'Y', '16', 'C', null, null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('SYS_VALID_TYPE', '有效标识符', 'app_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('UNIT_TYPE', '单位类型', 'app_code_define', null, null, null, null, null, null, null, null, null);
INSERT INTO "SYS_CODETYPE" VALUES ('USER_SEX', '性别类型', 'sys_code_define', null, 'N', 'Y', 'Y', '16', 'C', null, null, null);

-- ----------------------------
-- Table structure for SYS_FUNCTION
-- ----------------------------
DROP TABLE "SYS_FUNCTION";
CREATE TABLE "SYS_FUNCTION" (
"FUNC_ID" NVARCHAR2(36) NOT NULL ,
"FUNC_NAME" NVARCHAR2(64) NULL ,
"FUNC_TYPE" NVARCHAR2(32) NULL ,
"MAIN_HANDLER" NVARCHAR2(36) NULL ,
"FUNC_PID" NVARCHAR2(36) NULL ,
"FUNC_STATE" NCHAR(1) NULL ,
"FUNC_SORT" NUMBER(11) NULL ,
"FUNC_DESC" NVARCHAR2(256) NULL ,
"FUNC_ICON" NVARCHAR2(32) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SYS_FUNCTION
-- ----------------------------
INSERT INTO "SYS_FUNCTION" VALUES ('00000000-0000-0000-00000000000000000', '业务管理系统', 'funcmenu', null, null, '1', null, null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('00000000-0000-0000-00000000000000001', '系统管理', 'funcmenu', null, '00000000-0000-0000-00000000000000000', '1', '99', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('0855A94F-F8E0-42D6-A0DF-72F1D11C6D0B', '投票管理', 'funcnode', '87D4B614-DF7E-48D2-BA04-3A31728740D9', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '3', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('2F0F133F-1A6F-4E01-AADD-5B11C97BE6A0', '资源管理', 'funcnode', '6479AF01-D00B-4A40-B502-98FAF8418E92', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '5', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('36954650-E79A-4682-8AC2-D3A78461A3D4', '资源分组', 'funcnode', '0543DD98-8AE6-4DF1-990E-24CD51E73564', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '4', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('5FDEE3AB-6D32-4C5F-AD65-EF1EE5FFBAE6', '附件管理', 'funcnode', '1F617665-FC8B-4E8C-ABE4-540C363A17A8', '00000000-0000-0000-00000000000000001', '1', '8', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('67BA273A-DD31-48D0-B78C-1D60D5316074', '系统日志', 'funcnode', '494DF09B-7573-4CCA-85C1-97F4DC58C86B', '00000000-0000-0000-00000000000000001', '1', '7', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('692B0D37-2E66-4E82-92B4-E59BCF76EE76', '编码管理', 'funcnode', 'B4FE5722-9EA6-47D8-8770-D999A3F6A354', '00000000-0000-0000-00000000000000001', '1', '6', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('72D2FDBA-6400-4C48-A462-BA5E5B996631', '账户查询', 'funcnode', '6BA56996-4F31-464F-B230-A7E764CCC547', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '2', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('8C84B439-2788-4608-89C4-8F5AA076D124', '组织机构', 'funcnode', '439949F0-C6B7-49FF-8ED1-2A1B5062E7B9', '00000000-0000-0000-00000000000000001', '1', '1', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('A0334956-426E-4E49-831B-EB00E37285FD', '编码类型', 'funcnode', '9A16D554-F989-438A-B92D-C8C8AC6BF9B8', '00000000-0000-0000-00000000000000001', '1', '5', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '样例功能', 'funcmenu', null, '00000000-0000-0000-00000000000000000', '1', '100', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('C843A0B6-2837-48A6-9C16-DCB6FFB37790', '账户管理', 'funcnode', '048E2C48-1BD4-4EB2-94E4-208835D302EB', 'AD5188DB-BF4F-4CFB-853E-8CA469D3E477', '1', '1', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('C977BC31-C78F-4B16-B0C6-769783E46A06', '功能管理', 'funcnode', '46C52D33-8797-4251-951F-F7CA23C76BD7', '00000000-0000-0000-00000000000000001', '1', '4', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('D3582A2A-3173-4F92-B1AD-2F999A2CBE18', '修改密码', 'funcnode', '88882DB9-967F-430E-BA9C-D0BBBBD2BD0C', '00000000-0000-0000-00000000000000001', '1', '9', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('DFE8BE4C-4024-4A7B-8DF2-630003832AE9', '角色管理', 'funcnode', '0CE03AD4-FF29-4FDB-8FEE-DA8AA38B649F', '00000000-0000-0000-00000000000000001', '1', '2', null, null);
INSERT INTO "SYS_FUNCTION" VALUES ('E5DD18E9-1CF0-4BAA-9A29-03EC56441A2A', '人员管理', 'funcnode', '1FE570C2-56C2-4370-8E0A-AC68FEAAD5B7', '00000000-0000-0000-00000000000000001', '1', '3', null, null);

-- ----------------------------
-- Table structure for SYS_HANDLER
-- ----------------------------
DROP TABLE "SYS_HANDLER";
CREATE TABLE "SYS_HANDLER" (
"HANLER_ID" NVARCHAR2(36) NOT NULL ,
"HANLER_CODE" NVARCHAR2(64) NULL ,
"HANLER_TYPE" NVARCHAR2(32) NULL ,
"HANLER_URL" NVARCHAR2(128) NULL ,
"FUNC_ID" NVARCHAR2(36) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SYS_HANDLER
-- ----------------------------
INSERT INTO "SYS_HANDLER" VALUES ('048E2C48-1BD4-4EB2-94E4-208835D302EB', 'AccountManageList', 'MAIN', null, 'C843A0B6-2837-48A6-9C16-DCB6FFB37790');
INSERT INTO "SYS_HANDLER" VALUES ('0543DD98-8AE6-4DF1-990E-24CD51E73564', 'ResGroupTreeManage', 'MAIN', null, '36954650-E79A-4682-8AC2-D3A78461A3D4');
INSERT INTO "SYS_HANDLER" VALUES ('0CE03AD4-FF29-4FDB-8FEE-DA8AA38B649F', 'SecurityRoleTreeManage', 'MAIN', null, 'DFE8BE4C-4024-4A7B-8DF2-630003832AE9');
INSERT INTO "SYS_HANDLER" VALUES ('1157E31B-DA00-406C-BFCF-3BF5B588A5E0', 'SecurityUserManagerTreePick', 'OTHER', null, 'E5DD18E9-1CF0-4BAA-9A29-03EC56441A2A');
INSERT INTO "SYS_HANDLER" VALUES ('1D046723-D171-4FAE-BF4E-B1D26483E3E2', 'SecurityUserRGManageList', 'OTHER', null, 'E5DD18E9-1CF0-4BAA-9A29-03EC56441A2A');
INSERT INTO "SYS_HANDLER" VALUES ('1F617665-FC8B-4E8C-ABE4-540C363A17A8', 'WcmGeneralGroup8ContentList', 'MAIN', null, '5FDEE3AB-6D32-4C5F-AD65-EF1EE5FFBAE6');
INSERT INTO "SYS_HANDLER" VALUES ('1FE570C2-56C2-4370-8E0A-AC68FEAAD5B7', 'SecurityUserList', 'MAIN', null, 'E5DD18E9-1CF0-4BAA-9A29-03EC56441A2A');
INSERT INTO "SYS_HANDLER" VALUES ('439949F0-C6B7-49FF-8ED1-2A1B5062E7B9', 'SecurityGroupList', 'MAIN', null, '8C84B439-2788-4608-89C4-8F5AA076D124');
INSERT INTO "SYS_HANDLER" VALUES ('46C52D33-8797-4251-951F-F7CA23C76BD7', 'FunctionTreeManage', 'MAIN', null, 'C977BC31-C78F-4B16-B0C6-769783E46A06');
INSERT INTO "SYS_HANDLER" VALUES ('494DF09B-7573-4CCA-85C1-97F4DC58C86B', 'SysLogQueryList', 'MAIN', null, '67BA273A-DD31-48D0-B78C-1D60D5316074');
INSERT INTO "SYS_HANDLER" VALUES ('6479AF01-D00B-4A40-B502-98FAF8418E92', 'ResGroup8ContentList', 'MAIN', null, '2F0F133F-1A6F-4E01-AADD-5B11C97BE6A0');
INSERT INTO "SYS_HANDLER" VALUES ('6BA56996-4F31-464F-B230-A7E764CCC547', 'AccountQueryList', 'MAIN', null, '72D2FDBA-6400-4C48-A462-BA5E5B996631');
INSERT INTO "SYS_HANDLER" VALUES ('87D4B614-DF7E-48D2-BA04-3A31728740D9', 'VoteInfoManageList', 'MAIN', null, '0855A94F-F8E0-42D6-A0DF-72F1D11C6D0B');
INSERT INTO "SYS_HANDLER" VALUES ('88882DB9-967F-430E-BA9C-D0BBBBD2BD0C', 'ModifyPassword', 'MAIN', null, 'D3582A2A-3173-4F92-B1AD-2F999A2CBE18');
INSERT INTO "SYS_HANDLER" VALUES ('946CADBA-3007-4682-84DF-794BA0B2B4C6', 'SecurityUserQuery', 'OTHER', null, 'E5DD18E9-1CF0-4BAA-9A29-03EC56441A2A');
INSERT INTO "SYS_HANDLER" VALUES ('9A16D554-F989-438A-B92D-C8C8AC6BF9B8', 'CodeTypeManageList', 'MAIN', null, 'A0334956-426E-4E49-831B-EB00E37285FD');
INSERT INTO "SYS_HANDLER" VALUES ('B4FE5722-9EA6-47D8-8770-D999A3F6A354', 'CodeListManageList', 'MAIN', null, '692B0D37-2E66-4E82-92B4-E59BCF76EE76');

-- ----------------------------
-- Table structure for SYS_LOG
-- ----------------------------
DROP TABLE "SYS_LOG";
CREATE TABLE "SYS_LOG" (
"ID" NCHAR(36) NULL ,
"OPER_TIME" DATE NOT NULL ,
"IP_ADDTRESS" NVARCHAR2(32) NULL ,
"USER_ID" NVARCHAR2(32) NULL ,
"USER_NAME" NVARCHAR2(32) NULL ,
"FUNC_NAME" NVARCHAR2(64) NULL ,
"ACTION_TYPE" NVARCHAR2(32) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SYS_LOG
-- ----------------------------
INSERT INTO "SYS_LOG" VALUES ('F0FC2B42-FE14-4990-8B16-9BC7BBD1893B', TO_DATE('2018-01-16 15:45:41', 'YYYY-MM-DD HH24:MI:SS'), '127.0.0.1', 'admin', '管理员', '系统登陆', 'login');

-- ----------------------------
-- Table structure for SYS_ONLINECOUNT
-- ----------------------------
DROP TABLE "SYS_ONLINECOUNT";
CREATE TABLE "SYS_ONLINECOUNT" (
"IPADDRRESS" NVARCHAR2(64) NOT NULL ,
"ONLINECOUNT" NUMBER(11) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SYS_ONLINECOUNT
-- ----------------------------

-- ----------------------------
-- Table structure for SYS_OPERATION
-- ----------------------------
DROP TABLE "SYS_OPERATION";
CREATE TABLE "SYS_OPERATION" (
"OPER_ID" NCHAR(36) NOT NULL ,
"HANLER_ID" NVARCHAR2(36) NULL ,
"OPER_CODE" NVARCHAR2(64) NULL ,
"OPER_NAME" NVARCHAR2(64) NULL ,
"OPER_ACTIONTPYE" NVARCHAR2(64) NULL ,
"OPER_SORT" NUMBER(11) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of SYS_OPERATION
-- ----------------------------
INSERT INTO "SYS_OPERATION" VALUES ('3EC41FB8-ED98-41F3-B86D-F7473AEDD002', '494DF09B-7573-4CCA-85C1-97F4DC58C86B', 'viewDetail', '查看', 'viewDetail', '1');
INSERT INTO "SYS_OPERATION" VALUES ('6BF7A157-D333-4F40-8612-F61D3FD4D258', '494DF09B-7573-4CCA-85C1-97F4DC58C86B', 'refreshImgBtn', '刷新', 'refresh', '2');

-- ----------------------------
-- Table structure for WCM_GENERAL_GROUP
-- ----------------------------
DROP TABLE "WCM_GENERAL_GROUP";
CREATE TABLE "WCM_GENERAL_GROUP" (
"GRP_ID" NVARCHAR2(36) NOT NULL ,
"GRP_NAME" NVARCHAR2(64) NULL ,
"GRP_PID" NVARCHAR2(36) NULL ,
"GRP_ORDERNO" NUMBER(11) NULL ,
"GRP_IS_SYSTEM" NVARCHAR2(32) NULL ,
"GRP_RES_TYPE_DESC" NVARCHAR2(32) NULL ,
"GRP_RES_TYPE_EXTS" NVARCHAR2(128) NULL ,
"GRP_RES_SIZE_LIMIT" NVARCHAR2(32) NULL ,
"GRP_DESC" NVARCHAR2(128) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of WCM_GENERAL_GROUP
-- ----------------------------
INSERT INTO "WCM_GENERAL_GROUP" VALUES ('77777777-7777-7777-7777-777777777777', '附件目录', null, null, null, null, null, null, null);
INSERT INTO "WCM_GENERAL_GROUP" VALUES ('A6018D88-8345-46EE-A452-CE362FAC72E2', '视频文件', '77777777-7777-7777-7777-777777777777', '1', 'Y', '视频', '*.mp4;*.3gp;*.wmv;*.avi;*.rm;*.rmvb;*.flv', '100MB', null);
INSERT INTO "WCM_GENERAL_GROUP" VALUES ('CF35D1E6-102E-428A-B39C-0072D491D5B1', '业务附件', '77777777-7777-7777-7777-777777777777', '2', 'Y', '所有资源', '*.*', '2MB', null);

-- ----------------------------
-- Table structure for WCM_GENERAL_RESOURCE
-- ----------------------------
DROP TABLE "WCM_GENERAL_RESOURCE";
CREATE TABLE "WCM_GENERAL_RESOURCE" (
"RES_ID" NVARCHAR2(36) NOT NULL ,
"GRP_ID" NVARCHAR2(36) NULL ,
"RES_NAME" NVARCHAR2(64) NULL ,
"RES_SHAREABLE" NVARCHAR2(32) NULL ,
"RES_LOCATION" NVARCHAR2(256) NULL ,
"RES_SIZE" NVARCHAR2(64) NULL ,
"RES_SUFFIX" NVARCHAR2(32) NULL ,
"RES_DESCRIPTION" NVARCHAR2(256) NULL 
)
LOGGING
NOCOMPRESS
NOCACHE

;

-- ----------------------------
-- Records of WCM_GENERAL_RESOURCE
-- ----------------------------

-- ----------------------------
-- Indexes structure for table SECURITY_GROUP
-- ----------------------------

-- ----------------------------
-- Checks structure for table SECURITY_GROUP
-- ----------------------------
ALTER TABLE "SECURITY_GROUP" ADD CHECK ("GRP_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SECURITY_GROUP
-- ----------------------------
ALTER TABLE "SECURITY_GROUP" ADD PRIMARY KEY ("GRP_ID");

-- ----------------------------
-- Indexes structure for table SECURITY_GROUP_AUTH
-- ----------------------------

-- ----------------------------
-- Checks structure for table SECURITY_GROUP_AUTH
-- ----------------------------
ALTER TABLE "SECURITY_GROUP_AUTH" ADD CHECK ("GRP_AUTH_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SECURITY_GROUP_AUTH
-- ----------------------------
ALTER TABLE "SECURITY_GROUP_AUTH" ADD PRIMARY KEY ("GRP_AUTH_ID");

-- ----------------------------
-- Indexes structure for table SECURITY_RG_AUTH
-- ----------------------------

-- ----------------------------
-- Checks structure for table SECURITY_RG_AUTH
-- ----------------------------
ALTER TABLE "SECURITY_RG_AUTH" ADD CHECK ("RG_AUTH_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SECURITY_RG_AUTH
-- ----------------------------
ALTER TABLE "SECURITY_RG_AUTH" ADD PRIMARY KEY ("RG_AUTH_ID");

-- ----------------------------
-- Indexes structure for table SECURITY_ROLE
-- ----------------------------

-- ----------------------------
-- Checks structure for table SECURITY_ROLE
-- ----------------------------
ALTER TABLE "SECURITY_ROLE" ADD CHECK ("ROLE_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SECURITY_ROLE
-- ----------------------------
ALTER TABLE "SECURITY_ROLE" ADD PRIMARY KEY ("ROLE_ID");

-- ----------------------------
-- Indexes structure for table SECURITY_ROLE_AUTH
-- ----------------------------

-- ----------------------------
-- Checks structure for table SECURITY_ROLE_AUTH
-- ----------------------------
ALTER TABLE "SECURITY_ROLE_AUTH" ADD CHECK ("ROLE_AUTH_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SECURITY_ROLE_AUTH
-- ----------------------------
ALTER TABLE "SECURITY_ROLE_AUTH" ADD PRIMARY KEY ("ROLE_AUTH_ID");

-- ----------------------------
-- Indexes structure for table SECURITY_ROLE_GROUP_REL
-- ----------------------------

-- ----------------------------
-- Checks structure for table SECURITY_ROLE_GROUP_REL
-- ----------------------------
ALTER TABLE "SECURITY_ROLE_GROUP_REL" ADD CHECK ("RG_ID" IS NOT NULL);
ALTER TABLE "SECURITY_ROLE_GROUP_REL" ADD CHECK ("GRP_ID" IS NOT NULL);
ALTER TABLE "SECURITY_ROLE_GROUP_REL" ADD CHECK ("ROLE_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SECURITY_ROLE_GROUP_REL
-- ----------------------------
ALTER TABLE "SECURITY_ROLE_GROUP_REL" ADD PRIMARY KEY ("RG_ID");

-- ----------------------------
-- Indexes structure for table SECURITY_USER
-- ----------------------------

-- ----------------------------
-- Checks structure for table SECURITY_USER
-- ----------------------------
ALTER TABLE "SECURITY_USER" ADD CHECK ("USER_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SECURITY_USER
-- ----------------------------
ALTER TABLE "SECURITY_USER" ADD PRIMARY KEY ("USER_ID");

-- ----------------------------
-- Indexes structure for table SECURITY_USER_AUTH
-- ----------------------------

-- ----------------------------
-- Checks structure for table SECURITY_USER_AUTH
-- ----------------------------
ALTER TABLE "SECURITY_USER_AUTH" ADD CHECK ("USER_AUTH_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SECURITY_USER_AUTH
-- ----------------------------
ALTER TABLE "SECURITY_USER_AUTH" ADD PRIMARY KEY ("USER_AUTH_ID");

-- ----------------------------
-- Indexes structure for table SECURITY_USER_GROUP_REL
-- ----------------------------

-- ----------------------------
-- Checks structure for table SECURITY_USER_GROUP_REL
-- ----------------------------
ALTER TABLE "SECURITY_USER_GROUP_REL" ADD CHECK ("GU_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SECURITY_USER_GROUP_REL
-- ----------------------------
ALTER TABLE "SECURITY_USER_GROUP_REL" ADD PRIMARY KEY ("GU_ID");

-- ----------------------------
-- Indexes structure for table SECURITY_USER_RG_REL
-- ----------------------------

-- ----------------------------
-- Checks structure for table SECURITY_USER_RG_REL
-- ----------------------------
ALTER TABLE "SECURITY_USER_RG_REL" ADD CHECK ("URG_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SECURITY_USER_RG_REL
-- ----------------------------
ALTER TABLE "SECURITY_USER_RG_REL" ADD PRIMARY KEY ("URG_ID");

-- ----------------------------
-- Indexes structure for table SYS_CODELIST
-- ----------------------------

-- ----------------------------
-- Checks structure for table SYS_CODELIST
-- ----------------------------
ALTER TABLE "SYS_CODELIST" ADD CHECK ("TYPE_ID" IS NOT NULL);
ALTER TABLE "SYS_CODELIST" ADD CHECK ("CODE_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SYS_CODELIST
-- ----------------------------
ALTER TABLE "SYS_CODELIST" ADD PRIMARY KEY ("TYPE_ID", "CODE_ID");

-- ----------------------------
-- Indexes structure for table SYS_CODETYPE
-- ----------------------------

-- ----------------------------
-- Checks structure for table SYS_CODETYPE
-- ----------------------------
ALTER TABLE "SYS_CODETYPE" ADD CHECK ("TYPE_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SYS_CODETYPE
-- ----------------------------
ALTER TABLE "SYS_CODETYPE" ADD PRIMARY KEY ("TYPE_ID");

-- ----------------------------
-- Indexes structure for table SYS_FUNCTION
-- ----------------------------

-- ----------------------------
-- Checks structure for table SYS_FUNCTION
-- ----------------------------
ALTER TABLE "SYS_FUNCTION" ADD CHECK ("FUNC_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SYS_FUNCTION
-- ----------------------------
ALTER TABLE "SYS_FUNCTION" ADD PRIMARY KEY ("FUNC_ID");

-- ----------------------------
-- Indexes structure for table SYS_HANDLER
-- ----------------------------

-- ----------------------------
-- Checks structure for table SYS_HANDLER
-- ----------------------------
ALTER TABLE "SYS_HANDLER" ADD CHECK ("HANLER_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SYS_HANDLER
-- ----------------------------
ALTER TABLE "SYS_HANDLER" ADD PRIMARY KEY ("HANLER_ID");

-- ----------------------------
-- Checks structure for table SYS_LOG
-- ----------------------------
ALTER TABLE "SYS_LOG" ADD CHECK ("OPER_TIME" IS NOT NULL);

-- ----------------------------
-- Indexes structure for table SYS_ONLINECOUNT
-- ----------------------------

-- ----------------------------
-- Checks structure for table SYS_ONLINECOUNT
-- ----------------------------
ALTER TABLE "SYS_ONLINECOUNT" ADD CHECK ("IPADDRRESS" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SYS_ONLINECOUNT
-- ----------------------------
ALTER TABLE "SYS_ONLINECOUNT" ADD PRIMARY KEY ("IPADDRRESS");

-- ----------------------------
-- Indexes structure for table SYS_OPERATION
-- ----------------------------

-- ----------------------------
-- Checks structure for table SYS_OPERATION
-- ----------------------------
ALTER TABLE "SYS_OPERATION" ADD CHECK ("OPER_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table SYS_OPERATION
-- ----------------------------
ALTER TABLE "SYS_OPERATION" ADD PRIMARY KEY ("OPER_ID");

-- ----------------------------
-- Indexes structure for table WCM_GENERAL_GROUP
-- ----------------------------

-- ----------------------------
-- Checks structure for table WCM_GENERAL_GROUP
-- ----------------------------
ALTER TABLE "WCM_GENERAL_GROUP" ADD CHECK ("GRP_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table WCM_GENERAL_GROUP
-- ----------------------------
ALTER TABLE "WCM_GENERAL_GROUP" ADD PRIMARY KEY ("GRP_ID");

-- ----------------------------
-- Indexes structure for table WCM_GENERAL_RESOURCE
-- ----------------------------

-- ----------------------------
-- Checks structure for table WCM_GENERAL_RESOURCE
-- ----------------------------
ALTER TABLE "WCM_GENERAL_RESOURCE" ADD CHECK ("RES_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table WCM_GENERAL_RESOURCE
-- ----------------------------
ALTER TABLE "WCM_GENERAL_RESOURCE" ADD PRIMARY KEY ("RES_ID");
