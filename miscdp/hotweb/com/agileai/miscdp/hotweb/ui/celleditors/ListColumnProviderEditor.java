package com.agileai.miscdp.hotweb.ui.celleditors;

import org.eclipse.jface.viewers.DialogCellEditor;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;

import com.agileai.miscdp.hotweb.domain.CodeType;
import com.agileai.miscdp.hotweb.domain.ListTabColumn;
import com.agileai.miscdp.hotweb.ui.celleditors.dialog.ProviderDialog;
import com.agileai.miscdp.hotweb.ui.editors.BaseModelEditor;
/**
 * 列表中列属性的编辑器
 */
public class ListColumnProviderEditor extends DialogCellEditor{
	Table table = null;
	BaseModelEditor suModelEditor;
	TableViewer tableViewer;
    public ListColumnProviderEditor(BaseModelEditor suModelEditor,TableViewer tableViewer,Composite parent) {
        this(parent, SWT.NONE);
        this.table = (Table)parent;
        this.suModelEditor = suModelEditor;
        this.tableViewer = tableViewer;
    }
    public ListColumnProviderEditor(Composite parent, int style) {
        super(parent, style);
    }
	protected Object openDialogBox(Control cellEditorWindow) {
		String projectName = suModelEditor.getFuncModel().getProjectName();
		ProviderDialog dialog = new ProviderDialog(this.getControl().getShell(),projectName);
		TableItem[] tableItems = table.getSelection();
		TableItem item = (TableItem)tableItems[0];
		ListTabColumn listTabColumn = (ListTabColumn) item.getData(); 
		Object value = getValue();
		if (value != null){			
			dialog.setCurrentKey((String)value);
		}
		dialog.open();
		CodeType codeType = (CodeType)dialog.getKeyNamePair();
		if (!dialog.isCancelEdit()){
			listTabColumn.setMappingItem(codeType.getKey());
			suModelEditor.setModified(true);
			suModelEditor.fireDirty();
			tableViewer.refresh();
		}
		return codeType;
	}
}
