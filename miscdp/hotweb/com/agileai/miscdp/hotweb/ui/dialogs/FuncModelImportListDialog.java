package com.agileai.miscdp.hotweb.ui.dialogs;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.Path;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CheckboxTableViewer;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import com.agileai.miscdp.bpspoxy.FuncModelService;
import com.agileai.miscdp.bpspoxy.ModelField;
import com.agileai.miscdp.hotweb.domain.ProjectConfig;
import com.agileai.miscdp.util.MiscdpUtil;

public class FuncModelImportListDialog extends Dialog {
	private CheckboxTableViewer checkboxTableViewer;
	private List<String> selectedModelCodes = new ArrayList<String>();
	private ProjectConfig projectConfig = null;
	private List<String> importedModules = new ArrayList<String>();
	
	public FuncModelImportListDialog(Shell parentShell,String projectName) {
		super(parentShell);
		this.projectConfig = MiscdpUtil.getProjectConfig(projectName);
		String mainPkg = this.projectConfig.getMainPkg();
		String pathSegment = mainPkg.replaceAll("\\.", "/");
		File moduleFolder =  MiscdpUtil.getProjectFile(projectName, new Path("/src/"+pathSegment+"/module"));
		if (moduleFolder.exists() && moduleFolder.isDirectory()){
			File[] files = moduleFolder.listFiles();
			for (File file : files) {
				if (file.isDirectory()){
					importedModules.add(file.getName());
				}
			}
		}
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout(1, false));

		Table table = new Table(container,SWT.MULTI | SWT.BORDER | SWT.CHECK |SWT.FULL_SELECTION);
		table.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		table.setHeaderVisible(true);
		
		checkboxTableViewer = new CheckboxTableViewer(table);
		
		TableColumn codeColumn = new TableColumn(table, SWT.NONE);
		codeColumn.setWidth(197);
		codeColumn.setText(Messages.getString("FuncModelImportListDialog.CodeColumnText"));
		
		TableColumn nameColumn = new TableColumn(table, SWT.NONE);
		nameColumn.setWidth(159);
		nameColumn.setText(Messages.getString("FuncModelImportListDialog.NameColumnText"));
		
		checkboxTableViewer.setLabelProvider(new ModelFieldLabelProvider());
		checkboxTableViewer.setContentProvider(new ArrayContentProvider());
		checkboxTableViewer.setInput(buildModelFieldList());
		
		return container;
	}

	private List<ModelField> buildModelFieldList(){
		List<ModelField> result = new ArrayList<ModelField>();
		FuncModelService funcModelService = MiscdpUtil.getFuncModelService(projectConfig);
		List<ModelField> funcModelList = funcModelService.retrieveFuncModelList();
		for (ModelField modelField : funcModelList) {
			String modelCode = modelField.getCode();
			if (this.importedModules.contains(modelCode.toLowerCase())){
				continue;
			}
			result.add(modelField);
		}
		return result;
	}
	
	/**
	 * Create contents of the button bar.
	 * @param parent
	 */
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL , true);
		createButton(parent, IDialogConstants.CANCEL_ID,IDialogConstants.CANCEL_LABEL , false);
	}
	
	@Override
	protected void okPressed() {
		Object[] objects = (Object[])checkboxTableViewer.getCheckedElements();
		if (objects != null){
			for (int i=0;i < objects.length;i++){
				ModelField modelField = (ModelField)objects[i];	
				this.selectedModelCodes.add(modelField.getCode());
			}
		}
		super.okPressed();
	}	

	public List<String> getSelectedModelCodes() {
		return selectedModelCodes;
	}

	/**
	 * Return the initial size of the dialog.
	 */
	@Override
	protected Point getInitialSize() {
		return new Point(389, 472);
	}
}

class ModelFieldLabelProvider extends LabelProvider implements ITableLabelProvider {
	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof ModelField) {
			ModelField p = (ModelField) element;
			if (columnIndex == 0) {
				return p.getCode();
			} else if (columnIndex == 1) {
				return p.getName();
			}
		}
		return null;
	}

	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}
}
