package com.agileai.miscdp.hotweb.ui.dialogs;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
/**
 * 选择工程提示框
 */
public class PickProjctDialog extends Dialog{
	private String fSelection= null;
	private final String[] fAllowedStrings;
	private final int fInitialSelectionIndex;
	public PickProjctDialog(Shell parentShell,String[] comboStrings, int initialSelectionIndex) {
		super(parentShell);
		fAllowedStrings= comboStrings;
		fInitialSelectionIndex= initialSelectionIndex;
	}

	String getSelectedString(){
		return fSelection;
	}
	
	protected Control createDialogArea(Composite parent) {
		Composite composite = (Composite)super.createDialogArea(parent);
		composite.setSize(200,100);
		Composite innerComposite = new Composite(composite, SWT.NONE);
		innerComposite.setLayoutData(new GridData());
		GridLayout gl= new GridLayout();
		gl.numColumns= 2;
		innerComposite.setLayout(gl);
		
		Label label= new Label(innerComposite, SWT.NONE);
		label.setText("可选工程");
		label.setLayoutData(new GridData());

		final Combo combo= new Combo(innerComposite, SWT.READ_ONLY);
		for (int i = 0; i < fAllowedStrings.length; i++) {
			combo.add(fAllowedStrings[i]);
		}
		combo.select(fInitialSelectionIndex);
		fSelection= combo.getItem(combo.getSelectionIndex());
		GridData gd= new GridData(SWT.FILL, SWT.CENTER, false, false);
		combo.setLayoutData(gd);
		combo.addSelectionListener(new SelectionAdapter(){
			public void widgetSelected(SelectionEvent e) {
				fSelection= combo.getItem(combo.getSelectionIndex());
			}
		});
		applyDialogFont(composite);	
		return composite;
	}
	public String getFSelection() {
		return fSelection;
	}
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, IDialogConstants.OK_LABEL,
				true);
	}
	protected void configureShell(Shell newShell) {
		super.configureShell(newShell);
		newShell.setText("工程选择框");
	}
}
