package com.agileai.miscdp.hotweb.ui.actions.treefill;

import java.io.File;
import java.lang.reflect.InvocationTargetException;

import org.apache.commons.lang.StringUtils;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.IProgressService;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.hotweb.domain.ProjectConfig;
import com.agileai.miscdp.hotweb.domain.treefill.TreeFillFuncModel;
import com.agileai.miscdp.hotweb.ui.actions.BaseInitGridAction;
import com.agileai.miscdp.hotweb.ui.editors.treefill.TFMBasicConfigPage;
import com.agileai.miscdp.hotweb.ui.editors.treefill.TreeFillModelEditor;
import com.agileai.miscdp.hotweb.ui.editors.treefill.TreeFillModelEditorContributor;
import com.agileai.miscdp.util.DialogUtil;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.util.StringUtil;
/**
 * 初始化表中功能模型数据
 */
public class TFMInitGridAction extends BaseInitGridAction{
	private TFMBasicConfigPage qBasicConfigPage;
	
	public TFMInitGridAction() {
	}
	public void setContributor(TreeFillModelEditorContributor contributor) {
		this.contributor = contributor;
	}
	public void run() {
		IWorkbenchPage workbenchPage = contributor.getPage();
		IEditorPart editorPart = workbenchPage.getActiveEditor();
		modelEditor = (TreeFillModelEditor)editorPart;
		Shell shell = modelEditor.getSite().getShell();
		
		this.qBasicConfigPage = ((TreeFillModelEditor)modelEditor).getBasicConfigPage();
		
		String listSql = qBasicConfigPage.getListSqlText().getText();
		if (StringUtils.isBlank(listSql)){
			DialogUtil.showInfoMessage("列表Sql不能为空！");
			return;
		}

		String sqlMapFile = qBasicConfigPage.getSqlMapFileText().getText();
		if (StringUtils.isBlank(sqlMapFile)){
			DialogUtil.showInfoMessage("SqlMap文件不能为空！");
			return;
		}
		
		String nodeIdField = qBasicConfigPage.getNodeIdFieldText().getText(); 
		if (StringUtils.isBlank(nodeIdField)){
			DialogUtil.showInfoMessage("节点标识字段不能为空！");
			return;
		}		
		
		String funcSubPkg = qBasicConfigPage.getFuncSubPkgText().getText();
		if (StringUtils.isBlank(funcSubPkg)){
			DialogUtil.showInfoMessage("编码目录不能为空！");
			return;
		}
		
		boolean doInitGrid = MessageDialog.openConfirm(shell,"消息提示","自动填写可能会覆盖已有内容，是否继续!");
		if (!doInitGrid){
			return;
		}
		
        IProgressService progressService = PlatformUI.getWorkbench().getProgressService();
        IRunnableWithProgress runnable = new IRunnableWithProgress() {
			public void run(IProgressMonitor monitor) throws InvocationTargetException {
				try {
					doRun(monitor);
				} catch (Exception e) {
					throw new InvocationTargetException(e);
				} finally {
					monitor.done();
				}
			}
        };
        try {
			progressService.runInUI(PlatformUI.getWorkbench().getProgressService(),
					runnable,ResourcesPlugin.getWorkspace().getRoot());
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	public void doRun(IProgressMonitor monitor) {
		monitor.beginTask("init grid values....", 1000);
		monitor.subTask("init basic values....");
		monitor.worked(50);
		
		((TreeFillModelEditor)modelEditor).buildFuncModel();
		TreeFillFuncModel funcModel = ((TreeFillModelEditor)modelEditor).getFuncModel();
		try {
			String funcSubPkg = qBasicConfigPage.getFuncSubPkgText().getText();
			funcModel.setFuncSubPkg(funcSubPkg);
			String sqlMapFileFullPath = qBasicConfigPage.getSqlMapFileText().getText();
			File sqlMapFile = new File(sqlMapFileFullPath);
			String tempFileName = sqlMapFile.getName();
			String namepPrefix = tempFileName.substring(0,tempFileName.length()-4);
			funcModel.setListJspName(funcSubPkg +"/"+namepPrefix+".jsp");
			String serviceIdTemp = namepPrefix+"Service";
			funcModel.setServiceId(StringUtil.lowerFirst(serviceIdTemp));

			String projectName = funcModel.getProjectName();
			ProjectConfig projectConfig = new ProjectConfig();
			String configFile = MiscdpUtil.getCfgFileName(projectName,DeveloperConst.PROJECT_CFG_NAME);
			projectConfig.setConfigFile(configFile);
			projectConfig.initConfig();
			String mainPkg = projectConfig.getMainPkg();
			funcModel.setImplClassName(mainPkg+".module."+funcSubPkg+".service."+namepPrefix+"Impl");
			funcModel.setInterfaceName(mainPkg+".module."+funcSubPkg+".service."+namepPrefix);
		} catch (Exception e) {
			e.printStackTrace();
		}
		((TreeFillModelEditor)modelEditor).setFuncModel(funcModel);
		modelEditor.initValues();
		
		monitor.subTask("refresh viewer....");
		monitor.worked(900);
		
		return;
	}
}
