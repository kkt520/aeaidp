﻿package com.agileai.miscdp.hotweb.ui.editors;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.part.MultiPageEditorActionBarContributor;

import com.agileai.miscdp.hotweb.domain.BaseFuncModel;
import com.agileai.miscdp.hotweb.domain.FuncModule;
import com.agileai.miscdp.hotweb.domain.FuncProject;
import com.agileai.miscdp.hotweb.ui.editors.module.FuncModuleEditor;
import com.agileai.miscdp.hotweb.ui.editors.project.FuncProjectEditor;
import com.agileai.miscdp.hotweb.ui.views.FuncModelTreeView;
import com.agileai.miscdp.hotweb.ui.views.TreeObject;
import com.agileai.miscdp.hotweb.ui.views.TreeViewContentProvider;
/**
 * 基础操作模型贡献者
 */
abstract public class BaseModelEditorContributor extends MultiPageEditorActionBarContributor {
	protected IEditorPart activeEditorPart;
	protected Action initGridAction;
	protected Action genCodesAction;
	public BaseModelEditorContributor() {
		super();
		createActions();
	}
	public IEditorPart getActiveEditorPart() {
		return activeEditorPart;
	}
	public void setActiveEditor(IEditorPart editorPart) {
		super.setActiveEditor(editorPart);
		FuncModelTreeView funcModelTree = null;
		String funcId = null;
		String moduleId = null;
		String projectName = null;
		if (editorPart instanceof BaseModelEditor) {
			BaseFuncModel baseFuncModel = ((BaseModelEditor)editorPart).getFuncModel();
			funcId = baseFuncModel.getFuncId();
			projectName = baseFuncModel.getProjectName();
			funcModelTree = ((BaseModelEditor)editorPart).getFuncModelTree();
		}
		else if (editorPart instanceof FuncProjectEditor){
			FuncProject baseFuncModel = ((FuncProjectEditor)editorPart).getFuncProject();
			projectName = baseFuncModel.getProjectName();
			funcModelTree = ((FuncModuleEditor)editorPart).getFuncModelTree();
		}
		else if (editorPart instanceof FuncModuleEditor){
			FuncModule baseFuncModel = ((FuncModuleEditor)editorPart).getFuncModule();
			moduleId = baseFuncModel.getModuleId();
			projectName = baseFuncModel.getProjectName();
			funcModelTree = ((FuncModuleEditor)editorPart).getFuncModelTree();
		}
		else if (editorPart instanceof SimpleModelEditor) {
			BaseFuncModel baseFuncModel= ((SimpleModelEditor)editorPart).getFuncModel();
			funcId = baseFuncModel.getFuncId();
			projectName = baseFuncModel.getProjectName();
			funcModelTree = ((SimpleModelEditor)editorPart).getFuncModelTree();
		}
		if (funcModelTree != null){
			TreeViewer funcTreeViewer = funcModelTree.getFuncTreeViewer();
			TreeViewContentProvider contentProvider = (TreeViewContentProvider)funcTreeViewer.getContentProvider();
			TreeObject rootTreeObject = contentProvider.getProjectTreeObject(projectName);
			String currentId = funcId == null ? moduleId: funcId;
			TreeObject matchedTreeObject = null;
			if (currentId == null){
				matchedTreeObject = rootTreeObject;
			}else{
				matchedTreeObject = loopFindTreeObject(rootTreeObject,currentId);
			}
			ISelection newSelection = new StructuredSelection(new Object[]{matchedTreeObject});
			funcTreeViewer.setSelection(newSelection);
		}
	}
	private TreeObject loopFindTreeObject(TreeObject currentTreeObject,String targetId){
		TreeObject result = null;
		TreeObject[] treeObjects = currentTreeObject.getChildren();
		for (int i= 0;i < treeObjects.length;i++){
			TreeObject tempTreeObject = treeObjects[i];
			if (tempTreeObject.getId().equals(targetId)){
				result = tempTreeObject;
				break;
			}else{
				if (tempTreeObject.hasChildren()){
					return this.loopFindTreeObject(tempTreeObject, targetId);
				}
			}
		}
		return result;
	}
	public void setActivePage(IEditorPart part) {
		if (activeEditorPart == part){
			return;
		}else{
			activeEditorPart = part;
		}
	}
			
	abstract protected void createActions();
	abstract public void contributeToMenu(IMenuManager manager);
	abstract public void contributeToToolBar(IToolBarManager manager);
}
