package com.agileai.miscdp.hotweb.ui.editors.mastersub;

import java.util.List;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.agileai.miscdp.hotweb.domain.ListTabColumn;
import com.agileai.miscdp.hotweb.domain.mastersub.MasterSubFuncModel;
import com.agileai.miscdp.hotweb.domain.mastersub.SubTableConfig;
import com.agileai.miscdp.hotweb.ui.celleditors.ListColumnProviderEditor;
import com.agileai.miscdp.hotweb.ui.celleditors.TableViewerKeyboardSupporter;
import com.agileai.miscdp.hotweb.ui.editors.AddOrDelOperation;
import com.agileai.miscdp.hotweb.ui.editors.UpOrDownOperation;

public class SubTableListViewArea extends Composite {
	private Text findRecordsSql;
	private Text tabNameText;
	private TableViewer columnTableViewer;
	private Table columnTable;
	private MasterSubFuncModel funcModel= null;
	private SubTableConfig subTableConfig = null;
	private MasterSubModelEditor modelEditor = null;
	/**
	 * Create the composite
	 * @param parent
	 * @param style
	 */
	public SubTableListViewArea(final Composite parent, int style,final MasterSubModelEditor modelEditor,final SubTableConfig subTableConfig) {
		super(parent, style);
		this.funcModel = modelEditor.getFuncModel();
		this.subTableConfig = subTableConfig;
		this.modelEditor = modelEditor;
		
		final GridLayout gridLayout = new GridLayout();
		gridLayout.numColumns = 3;
		setLayout(gridLayout);

		final Label filteableLabel_1_1_1 = new Label(this, SWT.NONE);
		filteableLabel_1_1_1.setText("Tab名称");

		tabNameText = new Text(this, SWT.BORDER);
		tabNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(this, SWT.NONE);

		final Label listSqlLabel = new Label(this, SWT.NONE);
		listSqlLabel.setText("列表SQL");

		findRecordsSql = new Text(this, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.CANCEL | SWT.MULTI);
		final GridData gd_findRecordsSql = new GridData(SWT.FILL, SWT.CENTER, true, false);
		gd_findRecordsSql.heightHint = 70;
		findRecordsSql.setLayoutData(gd_findRecordsSql);

		final Composite composite = new Composite(this, SWT.NONE);
		composite.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false));
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.verticalSpacing = 0;
		gridLayout_1.marginWidth = 0;
		gridLayout_1.marginHeight = 0;
		gridLayout_1.horizontalSpacing = 0;
		composite.setLayout(gridLayout_1);

		final Button pboxBtn = new Button(composite, SWT.NONE);
		pboxBtn.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				SubEditPboxDialog configDialog = new SubEditPboxDialog(composite.getShell(),modelEditor,subTableConfig);
				configDialog.open();
			}
		});
		pboxBtn.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_RED));
		pboxBtn.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_RED));
		pboxBtn.setText("明细");
		final Label filteableLabel_1_1 = new Label(this, SWT.NONE);
		filteableLabel_1_1.setText("列表字段");

		columnTableViewer = new TableViewer(this,SWT.FULL_SELECTION | SWT.BORDER | SWT.V_SCROLL
				| SWT.H_SCROLL);
		columnTable = columnTableViewer.getTable();
		columnTable.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		columnTable.setHeaderVisible(true);
		columnTable.setLinesVisible(true);
		
		final TableColumn titleColumn = new TableColumn(columnTable, SWT.NONE);
		titleColumn.setWidth(120);
		titleColumn.setText("Title");
		
		final TableColumn propertyColumn = new TableColumn(columnTable, SWT.NONE);
		propertyColumn.setWidth(120);
		propertyColumn.setText("Property");

		final TableColumn widthColumn = new TableColumn(columnTable, SWT.NONE);
		widthColumn.setWidth(70);
		widthColumn.setText("Width");

		final TableColumn cellColumn = new TableColumn(columnTable, SWT.NONE);
		cellColumn.setWidth(70);
		cellColumn.setText("DataType");

		final TableColumn formatColumn = new TableColumn(columnTable, SWT.NONE);
		formatColumn.setWidth(120);
		formatColumn.setText("Format");

		final TableColumn mappingItemColumn = new TableColumn(columnTable, SWT.NONE);
		mappingItemColumn.setWidth(180);
		mappingItemColumn.setText("Provider");

		columnTableViewer.setLabelProvider(new TableColumnLabelProvider());
		columnTableViewer.setContentProvider(new ArrayContentProvider());
		
		
		final CellEditor[] tableColumnEditors = new CellEditor[columnTable.getColumnCount()]; 
		tableColumnEditors[0] = new TextCellEditor(columnTable);
		tableColumnEditors[1] = new TextCellEditor(columnTable);
		tableColumnEditors[2] = new ComboBoxCellEditor(columnTable,ListTabColumn.widths, SWT.READ_ONLY);
		tableColumnEditors[3] = new ComboBoxCellEditor(columnTable,ListTabColumn.dataTypes, SWT.READ_ONLY);
		tableColumnEditors[4] = new ComboBoxCellEditor(columnTable,ListTabColumn.formats, SWT.READ_ONLY);
		tableColumnEditors[5] = new ListColumnProviderEditor(modelEditor,columnTableViewer,columnTable);
		
		columnTableViewer.setCellEditors(tableColumnEditors);
		columnTableViewer.setColumnProperties(ListTabColumn.columnProperties); 
		
		TableViewerKeyboardSupporter columnTableViewerSupporter = new TableViewerKeyboardSupporter(columnTableViewer); 
		columnTableViewerSupporter.startSupport();
		
		columnTableViewer.setCellModifier(new ICellModifier(){
			public boolean canModify(Object element, String property) {
//				if (ListTabColumn.PROPERTY.equals(property)
//						|| ListTabColumn.CELL.equals(property)){
//					return false;
//				}
				return true;
			}
			public Object getValue(Object element, String property) {
				ListTabColumn listTabColumn = (ListTabColumn) element; 
				if (ListTabColumn.PROPERTY.equals(property)){
					return listTabColumn.getProperty();
				}
				else if (ListTabColumn.TITLE.equals(property)){
					return listTabColumn.getTitle();
				}
				else if (ListTabColumn.CELL.equals(property)){
					return getCellIndex(listTabColumn.getCell());
				}
				else if (ListTabColumn.FORMAT.equals(property)){
					return getFormatIndex(listTabColumn.getFormat());
				}
				else if (ListTabColumn.MAPPINGITEM.equals(property)){
					return listTabColumn.getMappingItem();
				}
				else if (ListTabColumn.WIDTH.equals(property)){
					return getWidthIndex(listTabColumn.getWidth());
				}
				return null;
			}
			private int getWidthIndex(String cell){
				int result = 0;
				for (int i=0;i < ListTabColumn.widths.length;i++){
					if (ListTabColumn.widths[i].equals(cell)){
						result = i;
						break;
					}
				}
				return result;
			}
			private int getCellIndex(String cell){
				int result = 0;
				for (int i=0;i < ListTabColumn.dataTypes.length;i++){
					if (ListTabColumn.dataTypes[i].equals(cell)){
						result = i;
						break;
					}
				}
				return result;
			}
			private int getFormatIndex(String cell){
				int result = 0;
				for (int i=0;i < ListTabColumn.formats.length;i++){
					if (ListTabColumn.formats[i].equals(cell)){
						result = i;
						break;
					}
				}
				return result;
			}
			public void modify(Object element, String property, Object value) {
				TableItem item = (TableItem)element;
				ListTabColumn listTabColumn = (ListTabColumn)item.getData();
				boolean changed = false;
				if (ListTabColumn.PROPERTY.equals(property)){
					if (!String.valueOf(value).equals(listTabColumn.getProperty())){
						listTabColumn.setProperty(String.valueOf(value));
						changed = true;
					}
				}
				else if (ListTabColumn.TITLE.equals(property)){
					if (!String.valueOf(value).equals(listTabColumn.getTitle())){
						listTabColumn.setTitle(String.valueOf(value));
						changed = true;
					}
				}
				else if (ListTabColumn.CELL.equals(property)){
					Integer index = (Integer)value;
					if (index != -1 && !ListTabColumn.dataTypes[index].equals(listTabColumn.getCell())){
						listTabColumn.setCell(ListTabColumn.dataTypes[index]);
						changed = true;
					}
				}
				else if (ListTabColumn.FORMAT.equals(property)){
					Integer index = (Integer)value;
					if (index != -1 && !ListTabColumn.formats[index].equals(listTabColumn.getFormat())){
						listTabColumn.setFormat(ListTabColumn.formats[index]);
						changed = true;
					}
				}
				else if (ListTabColumn.WIDTH.equals(property)){
					Integer index = (Integer)value;
					if (index != -1 && !ListTabColumn.widths[index].equals(listTabColumn.getWidth())){
						listTabColumn.setWidth(ListTabColumn.widths[index]);
						changed = true;
					}
				}
				if (changed){
					modelEditor.setModified(true);
					modelEditor.fireDirty();
					columnTableViewer.refresh();
				}
			}
		});
		String subTableId = this.subTableConfig.getSubTableId();
		List<ListTabColumn> subListTableColumns = this.funcModel.getSubListTableColumns(subTableId);
		columnTableViewer.setInput(subListTableColumns);
		
		final Composite composite_1 = new Composite(this, SWT.NONE);
		composite_1.setLayoutData(new GridData(SWT.LEFT, SWT.FILL, false, false));
		final GridLayout gridLayout_2 = new GridLayout();
		gridLayout_2.verticalSpacing = 0;
		gridLayout_2.marginWidth = 0;
		gridLayout_2.marginHeight = 0;
		gridLayout_2.horizontalSpacing = 0;
		composite_1.setLayout(gridLayout_2);

		final Button addrowButton = new Button(composite_1, SWT.NONE);
		addrowButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				new AddOrDelOperation().addRow(columnTableViewer,ListTabColumn.class);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		addrowButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		addrowButton.setText("添加");

		final Button deleteButton = new Button(composite_1, SWT.NONE);
		deleteButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				new AddOrDelOperation().delRow(columnTableViewer);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		deleteButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		deleteButton.setText("删除");

		final Button upButton_1 = new Button(composite_1, SWT.NONE);
		upButton_1.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int selectedIndex = columnTableViewer.getTable().getSelectionIndex();
				new UpOrDownOperation().moveUp(columnTableViewer, selectedIndex);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		upButton_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		upButton_1.setText("上移");

		final Button downButton_1 = new Button(composite_1, SWT.NONE);
		downButton_1.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int selectedIndex = columnTableViewer.getTable().getSelectionIndex();
				new UpOrDownOperation().moveDown(columnTableViewer, selectedIndex);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		downButton_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		downButton_1.setText("下移");
	}

	public void initValues(){
		tabNameText.setText(subTableConfig.getTabTitile());
		findRecordsSql.setText(subTableConfig.getQueryListSql());
	}
	
	public Text getFindRecordsSql() {
		return findRecordsSql;
	}

	public Text getTabNameText() {
		return tabNameText;
	}

	public void registryModifiers(){
		tabNameText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		findRecordsSql.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
	}
	
	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	protected void checkSubclass() {
		// Disable the check that prevents subclassing of SWT components
	}

}
