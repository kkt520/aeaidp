package com.agileai.miscdp.hotweb.ui.editors.treecontent;

import java.util.List;

import net.sf.swtaddons.autocomplete.combo.AutocompleteComboInput;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ComboBoxCellEditor;
import org.eclipse.jface.viewers.ICellModifier;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.forms.widgets.FormToolkit;

import com.agileai.miscdp.hotweb.database.DBManager;
import com.agileai.miscdp.hotweb.domain.treecontent.ContentTableInfo;
import com.agileai.miscdp.hotweb.domain.treecontent.TreeContentFuncModel;
import com.agileai.miscdp.hotweb.ui.celleditors.ContentTableInfoProviderEditor;
import com.agileai.miscdp.hotweb.ui.celleditors.TableViewerKeyboardSupporter;
import com.agileai.miscdp.hotweb.ui.dialogs.FieldSelectDialog;
import com.agileai.miscdp.hotweb.ui.editors.AddOrDelOperation;
import com.agileai.miscdp.hotweb.ui.editors.Modifier;
import com.agileai.miscdp.hotweb.ui.editors.UpOrDownOperation;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.miscdp.util.ResourceUtil;
import com.agileai.util.StringUtil;
/**
 * 基础配置页面
 */
public class TACBasicConfigPage extends Composite implements Modifier {
	private Text rootColumnIdText;
	private TreeContentModelEditor modelEditor = null;
	private Text funcNameText;
	private Text funcTypeText;
	
	private Text columnIdFieldText;
	private Text columnNameFieldText;
	private Text columnSortFieldText;
	private Text columnParentIdFieldText;
	
	private final FormToolkit toolkit = new FormToolkit(Display.getCurrent());
	
	private TACExtendAttribute extendAttribute;
	public TACExtendAttribute getExtendAttribute() {
		return extendAttribute;
	}
	private Combo tableNameCombo;

	private TreeContentFuncModel funcModel= null;
	
	private Button isManageTreeY;
	private Button isManageTreeN;
	private Combo treeEditModeCombo;
	private TableViewer tableViewer;
	
	private CellEditor[] contentTableInfoEditors = null;
	private Text funcSubPkgText;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void setTableComboValues(String schema) {
		String projectName = funcModel.getProjectName();
		List tableList = DBManager.getInstance(projectName).getTables(schema);
		String[] tables = (String[]) tableList.toArray(new String[0]);
		this.tableNameCombo.setItems(tables);
	}
	/**
	 * Create the composite
	 * @param parent
	 * @param style
	 */
	public TACBasicConfigPage(Composite parent,TreeContentModelEditor modelEditor, int style) {
		super(parent, style);
		toolkit.adapt(this);
//		toolkit.paintBordersFor(this);
		this.modelEditor = modelEditor;
		this.funcModel = modelEditor.getFuncModel();
		this.createContent();
	}
	private void createContent(){
		final GridLayout gridLayout = new GridLayout();
		gridLayout.verticalSpacing = 2;
		gridLayout.marginWidth = 2;
		gridLayout.marginHeight = 2;
		gridLayout.horizontalSpacing = 2;
		gridLayout.numColumns = 3;
		setLayout(gridLayout);

		final Label label_1 = new Label(this, SWT.NONE);
		label_1.setText("功能名称");

		funcNameText = new Text(this, SWT.BORDER);
		toolkit.adapt(funcNameText, true, true);
		funcNameText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(this, SWT.NONE);

		final Label label = new Label(this, SWT.NONE);
		label.setText("功能类型");

		funcTypeText = new Text(this, SWT.READ_ONLY | SWT.BORDER);
		toolkit.adapt(funcTypeText, true, true);
		funcTypeText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(this, SWT.NONE);
		
		Label label_3 = new Label(this, SWT.NONE);
		label_3.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		label_3.setText("目录编码");
		
		funcSubPkgText = new Text(this, SWT.BORDER | SWT.READ_ONLY);
		funcSubPkgText.setEditable(false);
		funcSubPkgText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		toolkit.adapt(funcSubPkgText, true, true);
		new Label(this, SWT.NONE);

		final Label tablenameLabel = new Label(this, SWT.NONE);
		tablenameLabel.setText("树分组表");

		tableNameCombo = new Combo(this, SWT.NONE);
		tableNameCombo.setForeground(ResourceUtil.getColor(255, 255, 255));
		tableNameCombo.setBackground(ResourceUtil.getColor(128, 0, 255));
		tableNameCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		new AutocompleteComboInput(tableNameCombo);
		new Label(this, SWT.NONE);
		
		Label label_4 = new Label(this, SWT.NONE);
		label_4.setText("关联信息");
		
		tableViewer = new TableViewer(this, SWT.MULTI | SWT.FULL_SELECTION | SWT.BORDER);
		Table table = tableViewer.getTable();
		table.setLinesVisible(true);
		table.setHeaderVisible(true);
		toolkit.adapt(table, true, true);
		final GridData gd_table = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_table.heightHint = 110;
		table.setLayoutData(gd_table);

		final TableColumn entityNameColumn = new TableColumn(table, SWT.NONE);
		entityNameColumn.setWidth(143);
		entityNameColumn.setText("实体表名");
		
		final TableColumn tableModeColumn = new TableColumn(table, SWT.NONE);
		tableModeColumn.setWidth(144);
		tableModeColumn.setText("关联模式");

		final TableColumn relFieldColumn = new TableColumn(table, SWT.NONE);
		relFieldColumn.setWidth(134);
		relFieldColumn.setText("外键字段");

		final TableColumn relTableNameColumn = new TableColumn(table, SWT.NONE);
		relTableNameColumn.setWidth(135);
		relTableNameColumn.setText("中间表名");

		tableViewer.setLabelProvider(new ContentTableInfoProvider());
		tableViewer.setContentProvider(new ArrayContentProvider());
		contentTableInfoEditors = new CellEditor[table.getColumnCount()]; 
		contentTableInfoEditors[0] = new ContentTableInfoProviderEditor(modelEditor,tableViewer,table);
		contentTableInfoEditors[1] = new ComboBoxCellEditor(table,ContentTableInfo.tableModes, SWT.READ_ONLY);
		
		final Composite composite_2 = new Composite(this, SWT.NONE);
		composite_2.setLayoutData(new GridData(SWT.LEFT, SWT.TOP, false, false));
		final GridLayout gridLayout_3 = new GridLayout();
		gridLayout_3.verticalSpacing = 0;
		gridLayout_3.marginWidth = 0;
		gridLayout_3.marginHeight = 0;
		gridLayout_3.horizontalSpacing = 0;
		composite_2.setLayout(gridLayout_3);
		toolkit.adapt(composite_2);

		tableViewer.setCellEditors(contentTableInfoEditors);
		tableViewer.setColumnProperties(ContentTableInfo.columnProperties); 
		
		TableViewerKeyboardSupporter columnTableViewerSupporter = new TableViewerKeyboardSupporter(tableViewer); 
		columnTableViewerSupporter.startSupport();
		
		tableViewer.setCellModifier(new ICellModifier(){
			public boolean canModify(Object element, String property) {
				return true;
			}
			
			public Object getValue(Object element, String property) {
				ContentTableInfo contentTableInfo = (ContentTableInfo) element; 
				if (ContentTableInfo.TABLE_NAME.equals(property)){
					return contentTableInfo.getTableName();
				}
				else if (ContentTableInfo.TABLE_MODE.equals(property)){
					return getEditModelIndex(contentTableInfo.getTableMode());
				}
				else if (ContentTableInfo.FOREIGN_KEY.equals(property)){
					return contentTableInfo.getColField();
				}
				else if (ContentTableInfo.RELATE_TABLE.equals(property)){
					return contentTableInfo.getRelTableName();
				}				
				return null;
			}
			
			private int getEditModelIndex(String editMode){
				int result = 0;
				for (int i=0;i < ContentTableInfo.tableModes.length;i++){
					if (ContentTableInfo.tableModes[i].equals(editMode)){
						result = i;
						break;
					}
				}
				return result;
			}
			
			public void modify(Object element, String property, Object value) {
				TableItem item = (TableItem)element;
				ContentTableInfo contentTableInfo = (ContentTableInfo)item.getData();
				boolean changed = false;
				if (ContentTableInfo.TABLE_MODE.equals(property)){
					Integer index = (Integer)value;
					if (index != -1 && !ContentTableInfo.tableModes[index].equals(contentTableInfo.getTableMode())){
						contentTableInfo.setTableMode(ContentTableInfo.tableModes[index]);
						changed = true;
					}
				}
				else if (ContentTableInfo.FOREIGN_KEY.equals(property)){
					if (!String.valueOf(value).equals(contentTableInfo.getColField())){
						contentTableInfo.setColField(String.valueOf(value));
						changed = true;
					}
				}
				else if (ContentTableInfo.RELATE_TABLE.equals(property)){
					if (!String.valueOf(value).equals(contentTableInfo.getRelTableName())){
						contentTableInfo.setRelTableName(String.valueOf(value));
						changed = true;
					}
				}				
				if (changed){
					modelEditor.setModified(true);
					modelEditor.fireDirty();
					tableViewer.refresh();
				}
			}
		});
		tableViewer.setInput(funcModel.getContentTableInfoList());
		
		final Button addrowButton = new Button(composite_2, SWT.NONE);
		addrowButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				new AddOrDelOperation().addRow(tableViewer,ContentTableInfo.class);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		addrowButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		toolkit.adapt(addrowButton, true, true);
		addrowButton.setText("添加");

		final Button deleteButton = new Button(composite_2, SWT.NONE);
		deleteButton.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				new AddOrDelOperation().delRow(tableViewer);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		deleteButton.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		toolkit.adapt(deleteButton, true, true);
		deleteButton.setText("删除");

		final Button upButton_1 = new Button(composite_2, SWT.NONE);
		upButton_1.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int selectedIndex = tableViewer.getTable().getSelectionIndex();
				new UpOrDownOperation().moveUp(tableViewer, selectedIndex);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		upButton_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		toolkit.adapt(upButton_1, true, true);
		upButton_1.setText("上移");

		final Button downButton_1 = new Button(composite_2, SWT.NONE);
		downButton_1.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				int selectedIndex = tableViewer.getTable().getSelectionIndex();
				new UpOrDownOperation().moveDown(tableViewer, selectedIndex);
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		downButton_1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false));
		toolkit.adapt(downButton_1, true, true);
		downButton_1.setText("下移");
		
		final Composite formComposite = new Composite(this, SWT.NONE);
		formComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 3, 1));
		final GridLayout gridLayout_4 = new GridLayout();
		gridLayout_4.marginHeight = 2;
		gridLayout_4.horizontalSpacing = 0;
		gridLayout_4.verticalSpacing = 0;
		gridLayout_4.marginWidth = 0;
		formComposite.setLayout(gridLayout_4);
		toolkit.adapt(formComposite, true, true);

		final TabFolder tabFolder = new TabFolder(formComposite, SWT.NONE);
		final GridData gd_tabFolder = new GridData(SWT.FILL, SWT.FILL, true, true);
		gd_tabFolder.heightHint = 287;
		tabFolder.setLayoutData(gd_tabFolder);
		toolkit.adapt(tabFolder, true, true);
		
		final TabItem tabItem_1 = new TabItem(tabFolder, SWT.NONE);
		tabItem_1.setText("基本属性");
		final Composite composite = new Composite(tabFolder, SWT.NONE);
		final GridLayout gridLayout_1 = new GridLayout();
		gridLayout_1.numColumns = 3;
		composite.setLayout(gridLayout_1);
		toolkit.adapt(composite);
		tabItem_1.setControl(composite);

		final Label label_2 = new Label(composite, SWT.NONE);
		toolkit.adapt(label_2, true, true);
		label_2.setText("节点ID字段");
		
		columnIdFieldText = new Text(composite, SWT.BORDER);
		columnIdFieldText.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				openFieldSelectDialog(columnIdFieldText);
			}
		});
		toolkit.adapt(columnIdFieldText, true, true);
		columnIdFieldText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(composite, SWT.NONE).setText("*");
		
		
		final Label label_2_1 = new Label(composite, SWT.NONE);
		toolkit.adapt(label_2_1, true, true);
		label_2_1.setText("节点名称字段");

		columnNameFieldText = new Text(composite, SWT.BORDER);
		columnNameFieldText.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				openFieldSelectDialog(columnNameFieldText);
			}
		});
		toolkit.adapt(columnNameFieldText, true, true);
		columnNameFieldText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(composite, SWT.NONE).setText("*");
		
		final Label label_2_1_1 = new Label(composite, SWT.NONE);
		toolkit.adapt(label_2_1_1, true, true);
		label_2_1_1.setText("父节点ID字段");

		columnParentIdFieldText = new Text(composite, SWT.BORDER);
		columnParentIdFieldText.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				openFieldSelectDialog(columnParentIdFieldText);
			}
		});
		toolkit.adapt(columnParentIdFieldText, true, true);
		columnParentIdFieldText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(composite, SWT.NONE).setText("*");
		
		final Label label_2_1_1_1 = new Label(composite, SWT.NONE);
		toolkit.adapt(label_2_1_1_1, true, true);
		label_2_1_1_1.setText("节点排序字段");
		
		columnSortFieldText = new Text(composite, SWT.BORDER);
		columnSortFieldText.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseDoubleClick(MouseEvent e) {
				openFieldSelectDialog(columnSortFieldText);
			}
		});
		toolkit.adapt(columnSortFieldText, true, true);
		columnSortFieldText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(composite, SWT.NONE).setText("*");
		
		final Label label_2_1_1_1_1 = new Label(composite, SWT.NONE);
		toolkit.adapt(label_2_1_1_1_1, true, true);
		label_2_1_1_1_1.setText("根节点指定值");

		rootColumnIdText = new Text(composite, SWT.BORDER);
		rootColumnIdText.setForeground(ResourceUtil.getColor(255, 255, 255));
		rootColumnIdText.setBackground(ResourceUtil.getColor(128, 0, 255));
		rootColumnIdText.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(composite, SWT.NONE).setText("*");

		final Label label_2_1_1_1_1_1 = new Label(composite, SWT.NONE);
		toolkit.adapt(label_2_1_1_1_1_1, true, true);
		label_2_1_1_1_1_1.setText("是否管理树");

		Composite isManageTreeGroup = new Composite(composite, SWT.NONE);
		toolkit.adapt(isManageTreeGroup);
		GridData gd_isManageTreeGroup = new GridData(SWT.FILL, SWT.TOP, false, false, 1, 1);
		gd_isManageTreeGroup.heightHint = 20;
		gd_isManageTreeGroup.widthHint = 0;
		isManageTreeGroup.setLayoutData(gd_isManageTreeGroup);
		GridLayout gl_isManageTreeGroup = new GridLayout(2, false);
		gl_isManageTreeGroup.marginTop = 1;
		gl_isManageTreeGroup.marginBottom = 1;
		gl_isManageTreeGroup.verticalSpacing = 0;
		gl_isManageTreeGroup.marginHeight = 0;
		gl_isManageTreeGroup.horizontalSpacing = 14;
		isManageTreeGroup.setLayout(gl_isManageTreeGroup);
		isManageTreeY = toolkit.createButton(isManageTreeGroup, "是", SWT.RADIO);
		isManageTreeY.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				getExtendAttribute().refreshTemplateCombo(modelEditor);
			}
		});

		isManageTreeN = new Button(isManageTreeGroup, SWT.RADIO);
		isManageTreeN.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent e) {
				getExtendAttribute().refreshTemplateCombo(modelEditor);
			}
		});
		isManageTreeN.setSelection(true);
		toolkit.adapt(isManageTreeN, true, true);
		isManageTreeN.setText("否");
		new Label(composite, SWT.NONE).setText("*");

		final Label label_2_1_1_1_1_1_1 = new Label(composite, SWT.NONE);
		toolkit.adapt(label_2_1_1_1_1_1_1, true, true);
		label_2_1_1_1_1_1_1.setText("树编辑模式");

		treeEditModeCombo = new Combo(composite, SWT.NONE);
		treeEditModeCombo.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				getExtendAttribute().refreshTemplateCombo(modelEditor);
			}
		});
		treeEditModeCombo.setItems(new String[] {"Popup", "Tabed"});
		toolkit.adapt(treeEditModeCombo, true, true);
		treeEditModeCombo.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));
		new Label(composite, SWT.NONE).setText("*");
		
		final TabItem tabItem = new TabItem(tabFolder, SWT.NONE);
		tabItem.setText("扩展属性");
		extendAttribute = new TACExtendAttribute(tabFolder,SWT.NONE);
		tabItem.setControl(extendAttribute);
		setTableComboValues(null);
	}
	
	private void openFieldSelectDialog(Text targetText){
		String tableName = tableNameCombo.getText();
		String projectName = funcModel.getProjectName();
		String[] fields = MiscdpUtil.getColNames(DBManager.getInstance(projectName), tableName);
		FieldSelectDialog dialog = new FieldSelectDialog(modelEditor,fields,targetText);
		dialog.open();
	}
	
	public void dispose() {
		super.dispose();
	}
	protected void checkSubclass() {
	}
	public Combo getTableNameCombo() {
		return tableNameCombo;
	}
	public TreeContentFuncModel buildConfig(){
		String tableName = tableNameCombo.getText();
		funcModel.setTreeTableName(tableName);
		funcModel.setFuncName(funcNameText.getText());
//		funcModel.setFuncType(funcTypeText.getText());
		funcModel.setFuncSubPkg(funcSubPkgText.getText());
		funcModel.setServiceId(extendAttribute.getServiceIdText().getText());
		funcModel.setImplClassName(extendAttribute.getImplClassNameText().getText());
		funcModel.setInterfaceName(extendAttribute.getInterfaceNameText().getText());
		funcModel.setListJspName(extendAttribute.getListJspNameText().getText());
		funcModel.setTemplate(extendAttribute.getTemplateCombo().getText());
		
		funcModel.setColumnIdField(columnIdFieldText.getText());
		funcModel.setColumnNameField(columnNameFieldText.getText());
		funcModel.setColumnParentIdField(columnParentIdFieldText.getText());
		funcModel.setColumnSortField(columnSortFieldText.getText());
		funcModel.setRootColumnId(rootColumnIdText.getText());
		boolean isManageTree = isManageTreeY.getSelection();
		funcModel.setManageTree(isManageTree);
		funcModel.setTreeEditMode(treeEditModeCombo.getText());
		
		return this.funcModel;
	}

	public void initValues() {
		int tableCount = this.tableNameCombo.getItemCount();
		if (funcModel.getTreeTableName() != null){
			for (int i=0;i < tableCount;i++){
				String tableName = this.tableNameCombo.getItem(i).toLowerCase();
				if (this.funcModel.getTreeTableName().toLowerCase().equals(tableName)){
					this.tableNameCombo.select(i);
					break;
				}
			}
		}
		funcNameText.setText(this.funcModel.getFuncName());
		funcTypeText.setText(this.funcModel.getFuncTypeName());
		funcSubPkgText.setText(this.funcModel.getFuncSubPkg());
		
		columnIdFieldText.setText(this.funcModel.getColumnIdField());
		columnNameFieldText.setText(this.funcModel.getColumnNameField());
		columnParentIdFieldText.setText(this.funcModel.getColumnParentIdField());
		columnSortFieldText.setText(this.funcModel.getColumnSortField());
		if (StringUtil.isNullOrEmpty(this.funcModel.getRootColumnId())){
			String rootId = "00000000-0000-0000-00000000000000000";
			this.funcModel.setRootColumnId(rootId);
			rootColumnIdText.setText(this.funcModel.getRootColumnId());
		}else{
			rootColumnIdText.setText(this.funcModel.getRootColumnId());			
		}
		boolean isManageTree = this.funcModel.isManageTree();
		if (isManageTree){
			isManageTreeY.setSelection(true);
			isManageTreeN.setSelection(false);
		}else{
			isManageTreeY.setSelection(false);
			isManageTreeN.setSelection(true);
		}
		treeEditModeCombo.setText(this.funcModel.getTreeEditMode());
		
		extendAttribute.getServiceIdText().setText(funcModel.getServiceId());
		extendAttribute.getImplClassNameText().setText(funcModel.getImplClassName());
		extendAttribute.getInterfaceNameText().setText(funcModel.getInterfaceName());
		extendAttribute.getListJspNameText().setText(funcModel.getListJspName());
		
		extendAttribute.refreshTemplateCombo(modelEditor);

	}
	public void registryModifier() {
		funcNameText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
//		funcSubPkgText.addModifyListener(new ModifyListener() {
//			public void modifyText(ModifyEvent e) {
//				if (!funcSubPkgText.getText().equals(funcModel.getFuncSubPkg())){
//					modelEditor.setModified(true);
//					modelEditor.fireDirty();				
//				}
//			}
//		});
		
		columnIdFieldText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		columnNameFieldText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		columnParentIdFieldText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		columnSortFieldText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		rootColumnIdText.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});		
		
		
		extendAttribute.getTemplateCombo().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		extendAttribute.getListJspNameText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		extendAttribute.getInterfaceNameText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		extendAttribute.getImplClassNameText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		extendAttribute.getServiceIdText().addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
		tableNameCombo.addModifyListener(new ModifyListener() {
			public void modifyText(ModifyEvent e) {
				modelEditor.setModified(true);
				modelEditor.fireDirty();
			}
		});
	}
	public Text getColumnIdFieldText() {
		return columnIdFieldText;
	}
	public Text getColumnNameFieldText() {
		return columnNameFieldText;
	}
	public Text getColumnSortFieldText() {
		return columnSortFieldText;
	}
	public Text getColumnParentIdFieldText() {
		return columnParentIdFieldText;
	}
	public Text getFuncSubPkgText() {
		return funcSubPkgText;
	}	
	public Button getIsManageTreeY() {
		return isManageTreeY;
	}
	public Button getIsManageTreeN() {
		return isManageTreeN;
	}
	public Combo getTreeEditModeCombo() {
		return treeEditModeCombo;
	}
	public TableViewer getTableViewer() {
		return tableViewer;
	}
}

class ContentTableInfoProvider extends LabelProvider implements ITableLabelProvider {
	public String getColumnText(Object element, int columnIndex) {
		if (element instanceof  ContentTableInfo) {
			ContentTableInfo contentTableInfo = (ContentTableInfo) element;
			if (columnIndex == 0) {
				return contentTableInfo.getTableName();
			} else if (columnIndex == 1) {
				return contentTableInfo.getTableMode();
			}
			else if (columnIndex == 2) {
				return contentTableInfo.getColField();
			}
			else if (columnIndex == 3) {
				return contentTableInfo.getRelTableName();
			}
		}
		return null;
	}
	public Image getColumnImage(Object element, int columnIndex) {
		return null;
	}
}
