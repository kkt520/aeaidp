package com.agileai.miscdp.hotweb.ui.editors;

import java.util.List;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.ide.IDE;
import org.eclipse.ui.part.MultiPageEditorPart;

import com.agileai.hotweb.model.BaseType;
import com.agileai.hotweb.model.CheckBoxType;
import com.agileai.hotweb.model.EditTableType;
import com.agileai.hotweb.model.FormObjectDocument.FormObject;
import com.agileai.hotweb.model.HiddenType;
import com.agileai.hotweb.model.LabelType;
import com.agileai.hotweb.model.ListTableType;
import com.agileai.hotweb.model.ListTableType.Row;
import com.agileai.hotweb.model.PopupType;
import com.agileai.hotweb.model.QueryBarType;
import com.agileai.hotweb.model.RadioType;
import com.agileai.hotweb.model.ResFileType;
import com.agileai.hotweb.model.SelectType;
import com.agileai.hotweb.model.TextAreaType;
import com.agileai.hotweb.model.TextType;
import com.agileai.hotweb.model.ValidateType;
import com.agileai.miscdp.hotweb.domain.BaseFuncModel;
import com.agileai.miscdp.hotweb.domain.ListTabColumn;
import com.agileai.miscdp.hotweb.domain.PageFormField;
import com.agileai.miscdp.hotweb.domain.PageParameter;
import com.agileai.miscdp.hotweb.domain.Validation;
import com.agileai.miscdp.hotweb.domain.treemanage.TreeManageFuncModel;
import com.agileai.miscdp.hotweb.ui.views.FuncModelTreeView;
import com.agileai.miscdp.hotweb.ui.views.TreeObject;
import com.agileai.miscdp.hotweb.ui.views.TreeViewContentProvider;
import com.agileai.miscdp.util.MiscdpUtil;
import com.agileai.util.StringUtil;
/**
 * 抽象模型编辑器，其他多页模型编辑继承该类
 */
public abstract class BaseModelEditor 
	extends MultiPageEditorPart implements IResourceChangeListener{
	protected boolean isModified = false;
	protected FuncModelTreeView funcModelTree = null;
	protected BaseFuncModel funcModel = null;
	
	public void doSaveAs() {
	}

	public boolean isSaveAsAllowed() {
		return false;
	}

	public void resourceChanged(IResourceChangeEvent arg0) {
	}
	public void setActivePage(int pageIndex) {
		super.setActivePage(pageIndex);
	}
	public void dispose() {
		ResourcesPlugin.getWorkspace().removeResourceChangeListener(this);
		super.dispose();
	}
	public boolean isDirty(){
		return this.isModified;
	}
	public void fireDirty(){
		firePropertyChange(PROP_DIRTY);
	}
	protected void pageChange(int newPageIndex) {
		super.pageChange(newPageIndex);
	}
	public void setFuncModelTree(FuncModelTreeView funcModelTree) {
		this.funcModelTree = funcModelTree;
	}
	public void setModified(boolean isModified) {
		this.isModified = isModified;
	}
	public void gotoMarker(IMarker marker) {
		setActivePage(0);
		IDE.gotoMarker(getEditor(0), marker);
	}
	public BaseFuncModel getFuncModel() {
		return funcModel;
	}
	
	abstract public BaseFuncModel buildFuncModel();
	abstract public void initValues();
	abstract public Object buildFuncModelDocument(BaseFuncModel suFuncModel);
	
	protected void updateIndexFile(String projectName){
		TreeViewer funcTreeViewer = funcModelTree.getFuncTreeViewer();
		TreeViewContentProvider treeViewContentProvider = (TreeViewContentProvider)funcTreeViewer.getContentProvider();
		TreeObject projectTreeObject = treeViewContentProvider.getProjectTreeObject(projectName);
		MiscdpUtil.updateTreeIndexFile(projectTreeObject);
	}

	public FuncModelTreeView getFuncModelTree() {
		return funcModelTree;
	}
	
	protected void processQueryBarType(QueryBarType queryBarType,List<PageParameter> pageParameters){
		if (pageParameters == null){
			return;
		}
		for (int i=0;i < pageParameters.size();i++){
			PageParameter pageParameter = (PageParameter)pageParameters.get(i);
			FormObject formObject = queryBarType.addNewFormObject();
			formObject.setLabel(pageParameter.getLabel());
			
			com.agileai.hotweb.model.FormObjectDocument.FormObject.DataType dataType = com.agileai.hotweb.model.FormObjectDocument.FormObject.DataType.Factory.newInstance();
			dataType.setStringValue(pageParameter.getDataType());
			formObject.xsetDataType(dataType);
			com.agileai.hotweb.model.FormObjectDocument.FormObject.Format format = com.agileai.hotweb.model.FormObjectDocument.FormObject.Format.Factory.newInstance();
			format.setStringValue(pageParameter.getFormat());
			formObject.xsetFormat(format);
			
			List<Validation> validations = pageParameter.getValidations();
			if (validations != null){
				int count = validations.size();
				for (int j=0;j < count;j++){
					ValidateType validateType  = formObject.addNewValidation();
					Validation validation = validations.get(j);
					ValidateType.Type type = ValidateType.Type.Factory.newInstance();
					type.setStringValue(validation.getKey());
					validateType.xsetType(type);
					validateType.setParam0(validation.getParam0());
				}
			}
			
			if ("text".equals(pageParameter.getTagType())){
				TextType textType = formObject.addNewText();
				textType.setId(pageParameter.getCode());
				textType.setName(pageParameter.getCode());
				textType.setSize(pageParameter.getSize());
				BaseType.DefValue defValue = textType.addNewDefValue();
				defValue.setValue(pageParameter.getDefaultValue());
				defValue.setIsVariable(pageParameter.isDefValueIsVar());
			}
			else if ("textarea".equals(pageParameter.getTagType())){
				TextAreaType textAreaType = formObject.addNewTextArea();
				textAreaType.setId(pageParameter.getCode());
				textAreaType.setName(pageParameter.getCode());
				BaseType.DefValue defValue = textAreaType.addNewDefValue();
				defValue.setValue(pageParameter.getDefaultValue());
				defValue.setIsVariable(pageParameter.isDefValueIsVar());
			}
			else if ("select".equals(pageParameter.getTagType())){
				SelectType selectType = formObject.addNewSelect();
				selectType.setId(pageParameter.getCode());
				selectType.setName(pageParameter.getCode());
				BaseType.DefValue defValue = selectType.addNewDefValue();
				defValue.setValue(pageParameter.getDefaultValue());
				defValue.setIsVariable(pageParameter.isDefValueIsVar());
				selectType.setValueProvider(pageParameter.getValueProvider());
			}
			else if ("radio".equals(pageParameter.getTagType())){
				RadioType radioType = formObject.addNewRadio();
				radioType.setId(pageParameter.getCode());
				radioType.setName(pageParameter.getCode());
				BaseType.DefValue defValue = radioType.addNewDefValue();
				defValue.setValue(pageParameter.getDefaultValue());
				defValue.setIsVariable(pageParameter.isDefValueIsVar());
				radioType.setValueProvider(pageParameter.getValueProvider());
			}
			else if ("checkbox".equals(pageParameter.getTagType())){
				CheckBoxType checkBoxType = formObject.addNewCheckBox();
				checkBoxType.setId(pageParameter.getCode());
				checkBoxType.setName(pageParameter.getCode());
				BaseType.DefValue defValue = checkBoxType.addNewDefValue();
				defValue.setValue(pageParameter.getDefaultValue());
				defValue.setIsVariable(pageParameter.isDefValueIsVar());
				checkBoxType.setValueProvider(pageParameter.getValueProvider());
			}	
			else if ("popup".equals(pageParameter.getTagType())){
				PopupType popupType = formObject.addNewPopup();
				popupType.setId(pageParameter.getCode());
				popupType.setName(pageParameter.getCode());
				popupType.setSize(pageParameter.getSize());
				BaseType.DefValue defValue = popupType.addNewDefValue();
				defValue.setValue(pageParameter.getDefaultValue());
				defValue.setIsVariable(pageParameter.isDefValueIsVar());
				popupType.setValueProvider(pageParameter.getValueProvider());
			}			
			else if ("hidden".equals(pageParameter.getTagType())){
				HiddenType hiddenType = formObject.addNewHidden();
				hiddenType.setId(pageParameter.getCode());
				hiddenType.setName(pageParameter.getCode());
				BaseType.DefValue defValue = hiddenType.addNewDefValue();
				defValue.setValue(pageParameter.getDefaultValue());
				defValue.setIsVariable(pageParameter.isDefValueIsVar());
			}
		}
	}
	
	protected void processEditTableType(EditTableType editTableType,List<PageFormField> pageFormFields){
		if (pageFormFields == null){
			return;
		}
		for (int i=0;i < pageFormFields.size();i++){
			PageFormField pageFormField = pageFormFields.get(i);
			FormObject formObject = editTableType.addNewFormObject();
			formObject.setLabel(pageFormField.getLabel());
			if (StringUtil.isNotNullNotEmpty(pageFormField.getDataType())){
				com.agileai.hotweb.model.FormObjectDocument.FormObject.DataType dataType = com.agileai.hotweb.model.FormObjectDocument.FormObject.DataType.Factory.newInstance();
				dataType.setStringValue(pageFormField.getDataType());
				formObject.xsetDataType(dataType);
			}
			
			com.agileai.hotweb.model.FormObjectDocument.FormObject.Format format = com.agileai.hotweb.model.FormObjectDocument.FormObject.Format.Factory.newInstance();
			format.setStringValue(pageFormField.getFormat());
			formObject.xsetFormat(format);
			
			List<Validation> validations = pageFormField.getValidations();
			if (validations != null){
				int count = validations.size();
				for (int j=0;j < count;j++){
					ValidateType validateType  = formObject.addNewValidation();
					Validation validation = validations.get(j);
					ValidateType.Type type = ValidateType.Type.Factory.newInstance();
					type.setStringValue(validation.getKey());
					validateType.xsetType(type);
					validateType.setParam0(validation.getParam0());
				}
			}
			
			if ("text".equals(pageFormField.getTagType())){
				TextType textType = formObject.addNewText();
				textType.setId(pageFormField.getCode());
				textType.setName(pageFormField.getCode());
				textType.setSize(pageFormField.getSize());
				BaseType.DefValue defValue = textType.addNewDefValue();
				defValue.setValue(pageFormField.getDefaultValue());
				defValue.setIsVariable(pageFormField.isDefValueIsVar());
			}
			else if ("textarea".equals(pageFormField.getTagType())){
				TextAreaType textAreaType = formObject.addNewTextArea();
				textAreaType.setId(pageFormField.getCode());
				textAreaType.setName(pageFormField.getCode());
				BaseType.DefValue defValue = textAreaType.addNewDefValue();
				defValue.setValue(pageFormField.getDefaultValue());
				defValue.setIsVariable(pageFormField.isDefValueIsVar());
			}
			else if ("select".equals(pageFormField.getTagType())){
				SelectType selectType = formObject.addNewSelect();
				selectType.setId(pageFormField.getCode());
				selectType.setName(pageFormField.getCode());
				BaseType.DefValue defValue = selectType.addNewDefValue();
				defValue.setValue(pageFormField.getDefaultValue());
				defValue.setIsVariable(pageFormField.isDefValueIsVar());
				selectType.setValueProvider(pageFormField.getValueProvider());
			}
			else if ("radio".equals(pageFormField.getTagType())){
				RadioType radio = formObject.addNewRadio();
				radio.setId(pageFormField.getCode());
				radio.setName(pageFormField.getCode());
				BaseType.DefValue defValue = radio.addNewDefValue();
				defValue.setValue(pageFormField.getDefaultValue());
				defValue.setIsVariable(pageFormField.isDefValueIsVar());
				radio.setValueProvider(pageFormField.getValueProvider());
			}
			else if ("checkbox".equals(pageFormField.getTagType())){
				CheckBoxType checkBox = formObject.addNewCheckBox();
				checkBox.setId(pageFormField.getCode());
				checkBox.setName(pageFormField.getCode());
				BaseType.DefValue defValue = checkBox.addNewDefValue();
				defValue.setValue(pageFormField.getDefaultValue());
				defValue.setIsVariable(pageFormField.isDefValueIsVar());
				checkBox.setValueProvider(pageFormField.getValueProvider());
			}	
			else if ("popup".equals(pageFormField.getTagType())){
				PopupType popup = formObject.addNewPopup();
				popup.setId(pageFormField.getCode());
				popup.setName(pageFormField.getCode());
				popup.setSize(pageFormField.getSize());
				BaseType.DefValue defValue = popup.addNewDefValue();
				defValue.setValue(pageFormField.getDefaultValue());
				defValue.setIsVariable(pageFormField.isDefValueIsVar());
				popup.setValueProvider(pageFormField.getValueProvider());
			}
			else if ("resfile".equals(pageFormField.getTagType())){
				ResFileType resFile = formObject.addNewResFile();
				resFile.setId(pageFormField.getCode());
				resFile.setName(pageFormField.getCode());
				BaseType.DefValue defValue = resFile.addNewDefValue();
				defValue.setValue(pageFormField.getDefaultValue());
				defValue.setIsVariable(pageFormField.isDefValueIsVar());
				resFile.setValueProvider(pageFormField.getValueProvider());
			}
			else if ("hidden".equals(pageFormField.getTagType())){
				HiddenType hiddenType = formObject.addNewHidden();
				hiddenType.setId(pageFormField.getCode());
				hiddenType.setName(pageFormField.getCode());
				hiddenType.setIsPK(pageFormField.getIsPK());
				BaseType.DefValue defValue = hiddenType.addNewDefValue();
				defValue.setValue(pageFormField.getDefaultValue());
				defValue.setIsVariable(pageFormField.isDefValueIsVar());
			}
			else if ("label".equals(pageFormField.getTagType())){
				LabelType labelType = formObject.addNewContent();
				labelType.setId(pageFormField.getCode());
				labelType.setName(pageFormField.getCode());
				BaseType.DefValue defValue = labelType.addNewDefValue();
				defValue.setValue(pageFormField.getDefaultValue());
				defValue.setIsVariable(pageFormField.isDefValueIsVar());
			}			
		}
	}
	
	protected void processEditTableType(TreeManageFuncModel funcModel,List<PageFormField> pageFormFields,EditTableType currentEditTableType,boolean ignoreParentField,String idNamePrefix){
		for (int i=0;i < pageFormFields.size();i++){
			PageFormField pageFormField = pageFormFields.get(i);
			String currentId = pageFormField.getCode();
			if (ignoreParentField && currentId.equals(funcModel.getParentIdField())){
				continue;
			}
			FormObject formObject = currentEditTableType.addNewFormObject();
			formObject.setLabel(pageFormField.getLabel());
			com.agileai.hotweb.model.FormObjectDocument.FormObject.DataType dataType = com.agileai.hotweb.model.FormObjectDocument.FormObject.DataType.Factory.newInstance();
			dataType.setStringValue(pageFormField.getDataType());
			formObject.xsetDataType(dataType);
			com.agileai.hotweb.model.FormObjectDocument.FormObject.Format format = com.agileai.hotweb.model.FormObjectDocument.FormObject.Format.Factory.newInstance();
			format.setStringValue(pageFormField.getFormat());
			formObject.xsetFormat(format);
			
			List<Validation> validations = pageFormField.getValidations();
			if (validations != null){
				int count = validations.size();
				for (int j=0;j < count;j++){
					ValidateType validateType  = formObject.addNewValidation();
					Validation validation = validations.get(j);
					ValidateType.Type type = ValidateType.Type.Factory.newInstance();
					type.setStringValue(validation.getKey());
					validateType.xsetType(type);
					validateType.setParam0(validation.getParam0());
				}
			}
			
			if ("text".equals(pageFormField.getTagType())){
				TextType textType = formObject.addNewText();
				textType.setId(idNamePrefix+pageFormField.getCode());
				textType.setName(idNamePrefix+pageFormField.getCode());
				textType.setSize(pageFormField.getSize());
				BaseType.DefValue defValue = textType.addNewDefValue();
				defValue.setValue(pageFormField.getDefaultValue());
				defValue.setIsVariable(pageFormField.isDefValueIsVar());
			}
			else if ("textarea".equals(pageFormField.getTagType())){
				TextAreaType textAreaType = formObject.addNewTextArea();
				textAreaType.setId(idNamePrefix+pageFormField.getCode());
				textAreaType.setName(idNamePrefix+pageFormField.getCode());
				BaseType.DefValue defValue = textAreaType.addNewDefValue();
				defValue.setValue(pageFormField.getDefaultValue());
				defValue.setIsVariable(pageFormField.isDefValueIsVar());
			}
			else if ("select".equals(pageFormField.getTagType())){
				SelectType selectType = formObject.addNewSelect();
				selectType.setId(idNamePrefix+pageFormField.getCode());
				selectType.setName(idNamePrefix+pageFormField.getCode());
				BaseType.DefValue defValue = selectType.addNewDefValue();
				defValue.setValue(pageFormField.getDefaultValue());
				defValue.setIsVariable(pageFormField.isDefValueIsVar());
				selectType.setValueProvider(pageFormField.getValueProvider());
			}
			else if ("radio".equals(pageFormField.getTagType())){
				RadioType radioType = formObject.addNewRadio();
				radioType.setId(idNamePrefix+pageFormField.getCode());
				radioType.setName(idNamePrefix+pageFormField.getCode());
				BaseType.DefValue defValue = radioType.addNewDefValue();
				defValue.setValue(pageFormField.getDefaultValue());
				defValue.setIsVariable(pageFormField.isDefValueIsVar());
				radioType.setValueProvider(pageFormField.getValueProvider());
			}	
			else if ("checkbox".equals(pageFormField.getTagType())){
				CheckBoxType checkBoxType = formObject.addNewCheckBox();
				checkBoxType.setId(idNamePrefix+pageFormField.getCode());
				checkBoxType.setName(idNamePrefix+pageFormField.getCode());
				BaseType.DefValue defValue = checkBoxType.addNewDefValue();
				defValue.setValue(pageFormField.getDefaultValue());
				defValue.setIsVariable(pageFormField.isDefValueIsVar());
				checkBoxType.setValueProvider(pageFormField.getValueProvider());
			}
			else if ("popup".equals(pageFormField.getTagType())){
				PopupType popupType = formObject.addNewPopup();
				popupType.setId(idNamePrefix+pageFormField.getCode());
				popupType.setName(idNamePrefix+pageFormField.getCode());
				popupType.setSize(pageFormField.getSize());
				BaseType.DefValue defValue = popupType.addNewDefValue();
				defValue.setValue(pageFormField.getDefaultValue());
				defValue.setIsVariable(pageFormField.isDefValueIsVar());
				popupType.setValueProvider(pageFormField.getValueProvider());
			}
			else if ("resfile".equals(pageFormField.getTagType())){
				ResFileType resFileType = formObject.addNewResFile();
				resFileType.setId(idNamePrefix+pageFormField.getCode());
				resFileType.setName(idNamePrefix+pageFormField.getCode());
				BaseType.DefValue defValue = resFileType.addNewDefValue();
				defValue.setValue(pageFormField.getDefaultValue());
				defValue.setIsVariable(pageFormField.isDefValueIsVar());
				resFileType.setValueProvider(pageFormField.getValueProvider());
			}			
			else if ("hidden".equals(pageFormField.getTagType())){
				HiddenType hiddenType = formObject.addNewHidden();
				hiddenType.setId(idNamePrefix+pageFormField.getCode());
				hiddenType.setName(idNamePrefix+pageFormField.getCode());
				hiddenType.setIsPK(pageFormField.getIsPK());
				BaseType.DefValue defValue = hiddenType.addNewDefValue();
				defValue.setValue(pageFormField.getDefaultValue());
				defValue.setIsVariable(pageFormField.isDefValueIsVar());
			}
		}
	}	
	protected void processListTableType(ListTableType listTableType,List<ListTabColumn> listTabColumns,String rsIdColumn) {
		if (listTabColumns != null){
			Row row = listTableType.addNewRow();
			if (!StringUtil.isNullOrEmpty(rsIdColumn)){
				row.setRsIdColumn(rsIdColumn);	
			}
			for (int i=0;i < listTabColumns.size();i++){
				ListTabColumn listTabColumn = listTabColumns.get(i);
				ListTableType.Row.Column column = row.addNewColumn();
				column.setCell(listTabColumn.getCell());
				column.setProperty(listTabColumn.getProperty());
				column.setFormat(listTabColumn.getFormat());
				column.setTitle(listTabColumn.getTitle());
				column.setWidth(listTabColumn.getWidth());
				column.setMappingItem(listTabColumn.getMappingItem());
			}
		}
	}	
}
