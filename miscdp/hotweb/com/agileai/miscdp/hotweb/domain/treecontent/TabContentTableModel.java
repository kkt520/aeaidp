package com.agileai.miscdp.hotweb.domain.treecontent;

public class TabContentTableModel {
	private String contentTabId = null;
	
	private boolean existRelateTable = false;
	
	private String findContentRecordsSQL = null;
	private String getContentRecordSQL = null;
	private String insertContentRecordSQL = null;
	private String updateContentRecordSQL = null;
	private String deleteContentRecordSQL = null;
	
	private String queryContentRelationSQL = null;
	private String insertContentRelationSQL = null;
	private String updateContentRelationSQL = null;
	private String deleteContentRelationSQL = null;
	
	public String getDeleteContentRecordSQL() {
		return deleteContentRecordSQL;
	}
	public void setDeleteContentRecordSQL(String deleteContentRecordSQL) {
		this.deleteContentRecordSQL = deleteContentRecordSQL;
	}
	public String getDeleteContentRelationSQL() {
		return deleteContentRelationSQL;
	}
	public void setDeleteContentRelationSQL(String deleteContentRelationSQL) {
		this.deleteContentRelationSQL = deleteContentRelationSQL;
	}
	public String getFindContentRecordsSQL() {
		return findContentRecordsSQL;
	}
	public void setFindContentRecordsSQL(String findContentRecordsSQL) {
		this.findContentRecordsSQL = findContentRecordsSQL;
	}
	public String getGetContentRecordSQL() {
		return getContentRecordSQL;
	}
	public void setGetContentRecordSQL(String getContentRecordSQL) {
		this.getContentRecordSQL = getContentRecordSQL;
	}
	public String getInsertContentRecordSQL() {
		return insertContentRecordSQL;
	}
	public void setInsertContentRecordSQL(String insertContentRecordSQL) {
		this.insertContentRecordSQL = insertContentRecordSQL;
	}
	public String getInsertContentRelationSQL() {
		return insertContentRelationSQL;
	}
	public void setInsertContentRelationSQL(String insertContentRelationSQL) {
		this.insertContentRelationSQL = insertContentRelationSQL;
	}
	public String getQueryContentRelationSQL() {
		return queryContentRelationSQL;
	}
	public void setQueryContentRelationSQL(String queryContentRelationSQL) {
		this.queryContentRelationSQL = queryContentRelationSQL;
	}
	public String getUpdateContentRecordSQL() {
		return updateContentRecordSQL;
	}
	public void setUpdateContentRecordSQL(String updateContentRecordSQL) {
		this.updateContentRecordSQL = updateContentRecordSQL;
	}
	public String getUpdateContentRelationSQL() {
		return updateContentRelationSQL;
	}
	public void setUpdateContentRelationSQL(String updateContentRelationSQL) {
		this.updateContentRelationSQL = updateContentRelationSQL;
	}
	public String getContentTabId() {
		return contentTabId;
	}
	public void setContentTabId(String contentTabId) {
		this.contentTabId = contentTabId;
	}
	public boolean isExistRelateTable() {
		return existRelateTable;
	}
	public void setExistRelateTable(boolean existRelateTable) {
		this.existRelateTable = existRelateTable;
	}
}
