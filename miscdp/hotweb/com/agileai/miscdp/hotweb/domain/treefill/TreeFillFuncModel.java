package com.agileai.miscdp.hotweb.domain.treefill;

import java.io.Reader;
import java.io.StringReader;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.hotweb.domain.BaseFuncModel;
import com.agileai.hotweb.model.treefill.TreeFillFuncModelDocument;
/**
 * 查询功能模型
 */
public class TreeFillFuncModel extends BaseFuncModel{
	
	private String selectTreeHandlerId = "";
	private String selectTreeHandlerClass = "";
	private String sqlMapFile = "";
	
	private boolean isMultiSelect = false;
	private String nodeIdField = "";
	private String nodeNameField = "";
	private String nodePIdField = "";
	private String nodeTypeField = "";
	
	private String rootIdParamKey = "";
	private String excludeIdParamKey = "";
	
	private String sqlNameSpace = "";
	
	public TreeFillFuncModel(){
		this.editorId = DeveloperConst.TREEFILL_EDITOR_ID;
	}
	public void buildFuncModel(String funcDefine){
		TreeFillFuncModel treeFillFuncModel = this;
		try {
			Reader reader = new StringReader(funcDefine);
			TreeFillFuncModelDocument funcModelDocument = TreeFillFuncModelDocument.Factory.parse(reader);
			
			TreeFillFuncModelDocument.TreeFillFuncModel.BaseInfo baseInfo = funcModelDocument.getTreeFillFuncModel().getBaseInfo();
			treeFillFuncModel.setSelectTreeHandlerId(baseInfo.getHandler().getSelectTreeHandlerId());
			treeFillFuncModel.setSelectTreeHandlerClass(baseInfo.getHandler().getSelectTreeHandlerClass());
			
			treeFillFuncModel.setServiceId(baseInfo.getService().getServiceId());
			treeFillFuncModel.setImplClassName(baseInfo.getService().getImplClassName());
			treeFillFuncModel.setInterfaceName(baseInfo.getService().getInterfaceName());
			treeFillFuncModel.setListJspName(baseInfo.getListJspName());
			
			treeFillFuncModel.setIsMultiSelect(baseInfo.getIsMultiSelect());
			treeFillFuncModel.setNodeIdField(baseInfo.getNodeIdField());
			treeFillFuncModel.setNodeNameField(baseInfo.getNodeNameField());
			treeFillFuncModel.setNodePIdField(baseInfo.getNodePIdField());
			treeFillFuncModel.setNodeTypeField(baseInfo.getNodeTypeField());
			
			treeFillFuncModel.setRootIdParamKey(baseInfo.getRootIdParamKey());
			treeFillFuncModel.setExcludeIdParamKey(baseInfo.getExcludeIdParamKey());
			
			treeFillFuncModel.setTemplate(baseInfo.getTemplate());
			treeFillFuncModel.setListSql(baseInfo.getQueryListSql());
			String sqlMapPath = baseInfo.getSqlResource().getSqlMapPath();
			treeFillFuncModel.setSqlMapFile(sqlMapPath);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public String getSqlMapFile() {
		return sqlMapFile;
	}
	public void setSqlMapFile(String sqlMapFile) {
		this.sqlMapFile = sqlMapFile;
	}
	public String getSqlNameSpace() {
		return sqlNameSpace;
	}
	public void setSqlNameSpace(String sqlNameSpace) {
		this.sqlNameSpace = sqlNameSpace;
	}

	public String getSelectTreeHandlerClass() {
		return selectTreeHandlerClass;
	}
	public void setSelectTreeHandlerClass(String selectTreeHandlerClass) {
		this.selectTreeHandlerClass = selectTreeHandlerClass;
	}
	public String getSelectTreeHandlerId() {
		return selectTreeHandlerId;
	}
	public void setSelectTreeHandlerId(String selectTreeHandlerId) {
		this.selectTreeHandlerId = selectTreeHandlerId;
	}
	public String getExcludeIdParamKey() {
		return excludeIdParamKey;
	}
	public void setExcludeIdParamKey(String excludeIdParamKey) {
		this.excludeIdParamKey = excludeIdParamKey;
	}
	public boolean getIsMultiSelect() {
		return isMultiSelect;
	}
	public void setIsMultiSelect(boolean isMultiSelect) {
		this.isMultiSelect = isMultiSelect;
	}
	public String getNodeIdField() {
		return nodeIdField;
	}
	public void setNodeIdField(String nodeIdField) {
		this.nodeIdField = nodeIdField;
	}
	public String getNodeNameField() {
		return nodeNameField;
	}
	public void setNodeNameField(String nodeNameField) {
		this.nodeNameField = nodeNameField;
	}
	public String getNodePIdField() {
		return nodePIdField;
	}
	public void setNodePIdField(String nodePIdField) {
		this.nodePIdField = nodePIdField;
	}
	public String getNodeTypeField() {
		return nodeTypeField;
	}
	public void setNodeTypeField(String nodeTypeField) {
		this.nodeTypeField = nodeTypeField;
	}
	public String getRootIdParamKey() {
		return rootIdParamKey;
	}
	public void setRootIdParamKey(String rootIdParamKey) {
		this.rootIdParamKey = rootIdParamKey;
	}
}
