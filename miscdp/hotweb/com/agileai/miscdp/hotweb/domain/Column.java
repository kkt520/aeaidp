﻿package com.agileai.miscdp.hotweb.domain;
/**
 * 数据表中的列对象
 */
public class Column {
	public static class PKType{
		public static final String INCREASE = "increase";
		public static final String ASSIGN = "assign";
		public static final String CHARGEN = "chargen";
	}
	
	private String name = null;
	private String label = null;
	private String typeName = null;
	private String className = null;
	private boolean nullable = true;
	private int length = 0;
	private boolean isPK = false;
	private int jdbcType = 0;
	private String jdbcTypeName = null;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String code) {
		this.label = code;
	}
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String dataType) {
		this.typeName = dataType;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public boolean isNullable() {
		return nullable;
	}
	public void setNullable(boolean nullable) {
		this.nullable = nullable;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public boolean isPK() {
		return isPK;
	}
	public void setPK(boolean isPK) {
		this.isPK = isPK;
	}
	public int getJdbcType() {
		return jdbcType;
	}
	public void setJdbcType(int jdbcType) {
		this.jdbcType = jdbcType;
	}
	public String getJdbcTypeName() {
		return jdbcTypeName;
	}
	public void setJdbcTypeName(String jdbcTypeName) {
		this.jdbcTypeName = jdbcTypeName;
	}
}