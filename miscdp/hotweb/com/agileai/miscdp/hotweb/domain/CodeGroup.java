﻿package com.agileai.miscdp.hotweb.domain;

import java.util.ArrayList;
import java.util.List;

import com.agileai.common.IniReader;
import com.agileai.domain.KeyNamePair;
import com.agileai.miscdp.util.MiscdpUtil;
/**
 * 编码分组
 */
public class CodeGroup extends KeyNamePair{
	@SuppressWarnings({"rawtypes" })
	private static List list = null;
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List prepareInput(){
		if (list == null){
			list = new ArrayList();
			IniReader reader = MiscdpUtil.getIniReader();
			List defValueList = reader.getList("CodeGroup");
			for (int i=0;i < defValueList.size();i++){
				KeyNamePair keyNamePair = (KeyNamePair)defValueList.get(i);
				list.add(new CodeGroup().put(keyNamePair.getKey(),keyNamePair.getValue()));					
			}
		}
		return list;
	}
}
