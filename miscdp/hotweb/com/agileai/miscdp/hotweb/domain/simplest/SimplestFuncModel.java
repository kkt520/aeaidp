package com.agileai.miscdp.hotweb.domain.simplest;

import java.io.Reader;
import java.io.StringReader;

import com.agileai.miscdp.DeveloperConst;
import com.agileai.miscdp.hotweb.domain.BaseFuncModel;
import com.agileai.hotweb.model.simplest.SimplestFuncModelDocument;
import com.agileai.util.StringUtil;

/**
 * 最简功能模型
 */
public class SimplestFuncModel extends BaseFuncModel {
	public static final String DEFULT_HANDLER_CLASS = "com.mis.web.handler.core.SimpleModelHandler";
	
	protected String handlerId = "";
	private String defaultJspName = "";
	private String defaultJspTitle = "";
	protected String handlerClass = "";
	
	public SimplestFuncModel(){
		this.editorId = DeveloperConst.SIMPLE_EDITOR_ID;
		this.handlerClass = DEFULT_HANDLER_CLASS;
		this.template = "Default";
	}

	public void buildFuncModel(String funcDefine) {
		SimplestFuncModel simplestFuncModel = this;
		try {
			Reader reader = new StringReader(funcDefine);
			SimplestFuncModelDocument funcModelDocument = SimplestFuncModelDocument.Factory.parse(reader);
			
			SimplestFuncModelDocument.SimplestFuncModel.BaseInfo baseInfo = funcModelDocument.getSimplestFuncModel().getBaseInfo();
			simplestFuncModel.setHandlerId(baseInfo.getHandler().getHandlerId());
			simplestFuncModel.setHandlerClass(baseInfo.getHandler().getHandlerClass());
			simplestFuncModel.setDefaultJspName(baseInfo.getDefaultJspName());
			simplestFuncModel.setDefaultJspTitle(baseInfo.getDefaultJspTitle());
			simplestFuncModel.setTemplate(baseInfo.getTemplate());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public String getDefaultJspName() {
		return defaultJspName;
	}
	public void setDefaultJspName(String defaultJspName) {
		if (defaultJspName != null)
		this.defaultJspName = defaultJspName;
	}
	public String getDefaultJspTitle() {
		if (StringUtil.isNullOrEmpty(defaultJspTitle)){
			return this.funcName;
		}else{
			return defaultJspTitle;			
		}
	}
	public void setDefaultJspTitle(String defaultJspTitle) {
		if (defaultJspTitle != null)
		this.defaultJspTitle = defaultJspTitle;
	}
	public String getHandlerId() {
		return handlerId;
	}
	public void setHandlerId(String handlerId) {
		if (handlerId != null)
		this.handlerId = handlerId;
	}
	public String getHandlerClass() {
		return handlerClass;
	}
	public void setHandlerClass(String handlerClass) {
		if (handlerClass != null)
		this.handlerClass = handlerClass;
	}
}
