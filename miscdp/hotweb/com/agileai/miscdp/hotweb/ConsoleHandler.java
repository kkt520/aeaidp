package com.agileai.miscdp.hotweb;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.swt.graphics.Color;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;

public class ConsoleHandler{  
    public static void info(final String _message){
    	try {
    		MessageConsole console = ConsoleFactory.getConsole();
        	MessageConsoleStream printer = console.newMessageStream();  
        	
        	printer.setActivateOnWrite(true);  
            printer.println(new SimpleDateFormat("HH:mm:ss").format(new Date())+ "(INFO)" +  " " + _message);      	
            printer.flush();	
            printer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }      
    public static void error(final String _message){  
    	try {
	    	MessageConsoleStream printer = ConsoleFactory.getConsole().newMessageStream();  
	    	printer.setActivateOnWrite(true);
	        printer.setColor(new Color(null,255,0,0));  
	        printer.println(new SimpleDateFormat("HH:mm:ss").format(new Date())+ "(ERROR)" + " " + _message);  
	        printer.flush();	
	        printer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }  
}