<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE sqlMap      
    PUBLIC "-//ibatis.apache.org//DTD SQL Map 2.0//EN"      
    "http://ibatis.apache.org/dtd/sql-map-2.dtd">
<sqlMap namespace="${table.namespace}">  
  <select id="queryRecords" parameterClass="com.agileai.domain.DataParam" resultClass="com.agileai.domain.DataRow">
	${table.queryRecordsSQL}
  </select>
  <select id="findDetail" parameterClass="com.agileai.domain.DataParam" resultClass="com.agileai.domain.DataRow">
    ${table.findDetailSQL}
  </select>
</sqlMap>