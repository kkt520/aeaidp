<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/mastersub",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro MasterSubFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local service = baseInfo.Service>
package ${Util.parsePkg(service.@ImplClassName)};

import java.util.*;
import com.agileai.hotweb.bizmoduler.core.MasterSubServiceImpl;
import ${service.@InterfaceName};

public class ${Util.parseClass(service.@ImplClassName)} extends MasterSubServiceImpl 
	implements ${Util.parseClass(service.@InterfaceName)} {
	
	public ${Util.parseClass(service.@ImplClassName)}(){
		super();
	}
	public String[] getTableIds() {
		List<String> temp = new ArrayList<String>();
		<#if baseInfo.@template="baseToped">
		<#list baseInfo.SubTableInfo as subTableInfo>temp.add("${subTableInfo.@subTableId}");</#list><#elseif baseInfo.@template="baseTabed">
		temp.add("_base");
		<#list baseInfo.SubTableInfo as subTableInfo>temp.add("${subTableInfo.@subTableId}");</#list></#if>
		return  temp.toArray(new String[]{});	
	}	
}
</#macro>