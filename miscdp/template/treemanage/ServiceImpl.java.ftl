<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/treemanage",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro TreeManageFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local service = baseInfo.Service>
package ${Util.parsePkg(service.@ImplClassName)};

import com.agileai.hotweb.bizmoduler.core.TreeManageImpl;
import ${service.@InterfaceName};

public class ${Util.parseClass(service.@ImplClassName)} extends TreeManageImpl 
	implements ${Util.parseClass(service.@InterfaceName)} {
	
	public ${Util.parseClass(service.@ImplClassName)}(){
		super();
		this.idField = "${baseInfo.@idField}";
		this.nameField = "${baseInfo.@nameField}";
		this.parentIdField = "${baseInfo.@parentIdField}";
		this.sortField = "${baseInfo.@sortField}";
	}
}
</#macro>