<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/treecontent",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro TreeContentFuncModel>
<#local baseInfo = .node.BaseInfo>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title></title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script language="javascript">
function saveTreeRecord(){
	if (!validate()){
		return;
	}
	showSplash();
	<#if (checkUnique)>
	postRequest('form1',{actionType:'checkUnique',onComplete:function(responseText){
		if (responseText == ''){
			postRequest('form1',{actionType:'save',onComplete:function(responseText){
				if ('success' == responseText){
					parent.refreshTree();
					parent.PopupBox.closeCurrent();
				}
			}});		
		}else{
			hideSplash();
			writeErrorMsg(responseText);
		}
	}});	
	<#else>
	postRequest('form1',{actionType:'save',onComplete:function(responseText){
		if ('success' == responseText){
			parent.refreshTree();
			parent.PopupBox.closeCurrent();
		}
	}});
	</#if>
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="saveTreeRecord()"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="javascript:parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<@Form.DetailEditArea paramArea=.node.TreeEditView.TreeEditArea></@Form.DetailEditArea>
</table>
<input type="hidden" name="actionType" id="actionType" value=""/>
<input type="hidden" name="operaType" id="operaType" value="<%=pageBean.getOperaType()%>"/>
<@Form.DetailHiddenArea hiddenArea=.node.TreeEditView.TreeEditArea></@Form.DetailHiddenArea>
</form>
<script language="javascript">
<@Form.Validation paramArea=.node.TreeEditView.TreeEditArea></@Form.Validation>
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
</#macro>
