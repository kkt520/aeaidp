<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/treecontent",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro TreeContentFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local service = baseInfo.Service>
package ${Util.parsePkg(service.@ImplClassName)};

import java.util.*;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManageImpl;
import ${service.@InterfaceName};

public class ${Util.parseClass(service.@ImplClassName)} extends TreeAndContentManageImpl 
	implements ${Util.parseClass(service.@InterfaceName)} {
	
	public ${Util.parseClass(service.@ImplClassName)}(){
		super();
    	this.columnIdField = "${baseInfo.@columnIdField}";
    	this.columnParentIdField = "${baseInfo.@columnParentIdField}";
    	this.columnSortField = "${baseInfo.@columnSortField}";			
	}
}
</#macro>