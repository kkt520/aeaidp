<#ftl ns_prefixes={"D":"http://www.hotweb.agileai.com/model/treecontent",
"fa":"http://www.hotweb.agileai.com/model"}>
<#import "/common/Util.ftl" as Util>
<#import "/common/PageForm.ftl" as Form>
<#visit doc>
<#macro TreeContentFuncModel>
<#local baseInfo = .node.BaseInfo>
<#local handler = baseInfo.Handler>
<#local service = baseInfo.Service>
<#local detailArea = .node.TreeEditView.TreeEditArea>
package ${Util.parsePkg(handler.@treeEditHandlerClass)};

import java.util.*;

import ${service.@InterfaceName};
import com.agileai.hotweb.controller.core.TreeAndContentColumnEditHandler;
import com.agileai.domain.*;
import com.agileai.hotweb.domain.*;

public class ${Util.parseClass(handler.@treeEditHandlerClass)} extends TreeAndContentColumnEditHandler{
	public ${Util.parseClass(handler.@treeEditHandlerClass)}(){
		super();
		this.serviceId = buildServiceId(${Util.parseClass(service.@InterfaceName)}.class);
		this.columnIdField = "${baseInfo.@columnIdField}";
    	this.columnParentIdField = "${baseInfo.@columnParentIdField}";
	}
	protected void processPageAttributes(DataParam param) {
		<#compress>
		<#if Util.isValid(detailArea)><#list detailArea["fa:FormObject"] as formObject><@SetAttribute formAtom=formObject/>
		</#list></#if>
		</#compress>
		
	}
	protected ${Util.parseClass(service.@InterfaceName)} getService() {
		return (${Util.parseClass(service.@InterfaceName)})this.lookupService(this.getServiceId());
	}
}
</#macro>

<#macro SetAttribute formAtom><#local type=formAtom.*[0]?node_name?lower_case><#local atom = formAtom.*[0]><#if (type="select" || type="radio" || type="checkbox")>
	setAttribute("${atom["fa:Name"]}",FormSelectFactory.create("${atom["fa:ValueProvider"]}").addSelectedValue(getOperaAttributeValue("${atom["fa:Name"]}","${atom["fa:DefValue"].@value}")));</#if>
</#macro>