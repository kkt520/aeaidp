package com.agileai.miscdp;

import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PlatformUI;

public class Application implements IApplication {
	public Object start(IApplicationContext context) throws Exception {
		System.setProperty("file.encoding", "UTF-8");
		Display display = PlatformUI.createDisplay();
		try {
			int returnCode = PlatformUI.createAndRunWorkbench(display,new ApplicationWorkbenchAdvisor());
			if (returnCode == 1) {
				return IApplication.EXIT_RESTART;
			}
			Integer localInteger = IApplication.EXIT_OK;
			return localInteger;
		} finally {
			display.dispose();
		}
	}
	
    @Override
    public void stop() {
        if (!PlatformUI.isWorkbenchRunning())
            return;
        final IWorkbench workbench = PlatformUI.getWorkbench();
        final Display display = workbench.getDisplay();
        display.syncExec(new Runnable() {
            public void run() {
                if (!display.isDisposed())
                    workbench.close();
            }
        });
    }
}