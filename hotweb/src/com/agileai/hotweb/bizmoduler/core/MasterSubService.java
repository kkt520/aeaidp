package com.agileai.hotweb.bizmoduler.core;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;

public interface MasterSubService extends BaseInterface{
	public static final String BASE_TABLE_ID = "_base";
	
	void deleteClusterRecords(DataParam param);
	void createMasterRecord(DataParam param);
	void createMasterRecord(DataParam param,String primaryKey);
	void updateMasterRecord(DataParam param);
	DataRow getMasterRecord(DataParam param);
	List<DataRow> findMasterRecords(DataParam param);
	
	void deleteSubRecord(String subId,DataParam param);
	void saveSubRecords(DataParam param,List<DataParam> insertRecords,List<DataParam> updateRecords);
	List<DataRow> findSubRecords(String subId,DataParam param);
	
	void createSubRecord(String subId,DataParam param);
	void updateSubRecord(String subId,DataParam param);
	DataRow getSubRecord(String subId,DataParam param);
	
	boolean isLastChild(String subId,DataParam param);
	boolean isFirstChild(String subId,DataParam param);
	void changeCurrentSort(DataParam param,boolean isUp);
	
	String[] getTableIds();
}
