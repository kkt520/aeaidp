package com.agileai.hotweb.bizmoduler.core;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;

public interface TreeManage extends BaseInterface,TreeSelectService{
	List<DataRow> findTreeRecords(DataParam param);
	List<DataRow> queryChildRecords(String currentId);
	DataRow queryCurrentRecord(DataParam param);
	void insertChildRecord(DataParam param);
	void copyCurrentRecord(String currentId);
	void updateCurrentRecord(DataParam param);
	void deleteCurrentRecord(String currentId);
	boolean isLastChild(DataParam param);
	boolean isFirstChild(DataParam param);
	void changeCurrentSort(String currentId,boolean isUp);
}
