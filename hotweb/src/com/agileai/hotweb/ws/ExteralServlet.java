package com.agileai.hotweb.ws;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.cxf.Bus;
import org.apache.cxf.BusFactory;
import org.apache.cxf.transport.servlet.CXFNonSpringServlet;

import com.agileai.hotweb.common.ExteralManager;
import com.agileai.util.StringUtil;

public class ExteralServlet extends CXFNonSpringServlet{
	private static final long serialVersionUID = -6379209129479102984L;
	private ServletConfig servletConfig = null;
	
	public void loadBus(ServletConfig servletConfig) {
		this.servletConfig = servletConfig;
		
		super.loadBus(servletConfig);
        Bus bus = this.getBus();
        BusFactory.setDefaultBus(bus);
        
        registryServices(false);
	}
	
	private void registryServices(boolean resetThreadDefaultBus){
		if (resetThreadDefaultBus){
			BusFactory.setThreadDefaultBus(getBus());
        }
		ExteralManager exteralManager = ExteralManager.getOnly(getServletContext());
		
        List<ServiceModel> soapModels = this.buildSoapServiceModelList(exteralManager);
        for (int i=0;i < soapModels.size();i++){
        	ServiceModel serviceModel = soapModels.get(i);
        	exteralManager.registrySoapService(serviceModel);
        }
        
        List<ServiceModel> restModels = this.buildRestServiceModelList(exteralManager);
        for (int i=0;i < restModels.size();i++){
        	ServiceModel serviceModel = restModels.get(i);
        	exteralManager.registryRestService(serviceModel);
        }
	}
	
	private List<ServiceModel> buildSoapServiceModelList(ExteralManager exteralManager){
		List<ServiceModel> models = new ArrayList<ServiceModel>();
		String contextPath = servletConfig.getServletContext().getContextPath();
		String appName = contextPath.substring(1);
		List<ServiceModel> soapServiceModels = exteralManager.getSoapServiceModels(appName);
		
		for (int i=0;i < soapServiceModels.size();i++){
			ServiceModel serviceModel = soapServiceModels.get(i);
			if (serviceModel != null){
				models.add(serviceModel);
			}
		}
		return models;
	}
	
	private List<ServiceModel> buildRestServiceModelList(ExteralManager exteralManager){
		List<ServiceModel> models = new ArrayList<ServiceModel>();
		String contextPath = servletConfig.getServletContext().getContextPath();
		String appName = contextPath.substring(1);
		
		List<ServiceModel> restServiceModels = exteralManager.getRestServiceModels(appName);
		
		for (int i=0;i < restServiceModels.size();i++){
			ServiceModel serviceModel = restServiceModels.get(i);
			if (serviceModel != null){
				models.add(serviceModel);
			}
		}
		return models;
	}
	
	private void setupCharset(HttpServletRequest request, HttpServletResponse response) throws ServletException{
		try {
			String charset = null;
			String charEncoding = request.getCharacterEncoding();
			if (StringUtil.isNullOrEmpty(charEncoding)) {
				charset = "UTF-8";
			}else{
				charset = charEncoding;
			}
			response.setHeader("content-type","text/xml;charset="+charset);
			response.setCharacterEncoding(charset);
		    request.setCharacterEncoding(charset);
		} catch (Exception e) {
			throw new ServletException(e);
		}
	}
	
	public void invoke(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		setupCharset(request, response);
		super.invoke(request, response);
	}
	
	@Override
	public void destroy() {
		super.destroy();
		try {
			ExteralManager exteralManager = ExteralManager.getOnly(getServletContext());
			exteralManager.destroyServices();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
}
