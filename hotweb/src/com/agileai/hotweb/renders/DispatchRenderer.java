package com.agileai.hotweb.renders;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.filter.XssRequestWrapper;
import com.agileai.util.StringUtil;

public class DispatchRenderer extends ViewRenderer {
	private String dispatchUrl = null;
	public DispatchRenderer(String dispatchUrl){
		this.dispatchUrl = dispatchUrl;
	}
	public void executeRender(HttpServlet httpServlet,HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException{
		String url = this.dispatchUrl;
		if (!this.dispatchUrl.startsWith("/")){
			if (this.dispatchUrl.equals("http://")
					|| this.dispatchUrl.equals("https://")){
				url = this.dispatchUrl;
			}else{
				url = "/" + this.dispatchUrl;
			}
		}
		String actionType = request.getParameter(BaseHandler.ACTION_TYPE);
		if (url.indexOf(BaseHandler.ACTION_TYPE) == -1 
				&& !StringUtil.isNullOrEmpty(actionType)){
			url = url + "&"+BaseHandler.ACTION_TYPE+"=";
		}

		if (request instanceof XssRequestWrapper) {
			int index = url.indexOf("&");
			if (index > 0) {
				String pathParamString = url.substring(index+1);
				String[] paramPairs = pathParamString.split("&");
				for (int i=0;i < paramPairs.length;i++) {
					String paramPair = paramPairs[i];
					String[] keyValuePair = paramPair.split("=");
					if (keyValuePair.length > 0) {
						if (keyValuePair.length > 1) {
							String value = XssRequestWrapper.stripXSS(keyValuePair[1]);
							request.getParameterMap().put(keyValuePair[0],new String[]{value});
						}
						else {
							request.getParameterMap().put(keyValuePair[0],new String[]{""});
						}
					}
				}
			}			
		}
		
		RequestDispatcher requestDispatcher = httpServlet.getServletContext().getRequestDispatcher(url);
		requestDispatcher.forward(request, response);
	}
}
