package com.agileai.hotweb.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.agileai.util.StringUtil;

import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisPubSub;

public class RedisHelper {
	private JedisPoolConfig jedisPoolConfig = null;
	private String hosts = null;
	private JedisCluster jedisCluster = null;
	private JedisPool jedisPool = null;
	private boolean clusterMode = false;
	private static RedisHelper instance = null;
	
	public static RedisHelper getOnly(JedisPoolConfig jedisPoolConfig,String hosts) {
		if (instance == null) {
			instance = new RedisHelper(jedisPoolConfig, hosts);
		}
		return instance;
	}
	private RedisHelper(JedisPoolConfig jedisPoolConfig,String hosts){
		this.jedisPoolConfig = jedisPoolConfig;
		this.hosts = hosts;
		
		Set<HostAndPort> jedisClusterNodes = new HashSet<HostAndPort>();
        String[] hostsArray = this.hosts.split(",");
        if (hostsArray.length > 1){
        	this.clusterMode = true;
        	
        	for (int i=0;i < hostsArray.length;i++){
            	String host = hostsArray[i];
            	String[] hostArray = host.split(":");
            	String ip = hostArray[0].trim();
            	int port = Integer.parseInt(hostArray[1].trim());
            	jedisClusterNodes.add(new HostAndPort(ip, port));
            }
        	this.jedisCluster = new JedisCluster(jedisClusterNodes,this.jedisPoolConfig);
        }else{
        	this.clusterMode = false;
        	
    		String host = hostsArray[0];
        	String[] hostArray = host.split(":");
        	String ip = hostArray[0].trim();
        	int port = Integer.parseInt(hostArray[1].trim());
        	
    		this.jedisPool = new JedisPool(jedisPoolConfig,ip,port);
        }
	}
	
	public boolean setObject(String key,Serializable serializable){
        boolean result = false;
        Jedis jedis = null;
        try {
    		ByteArrayOutputStream baos = new ByteArrayOutputStream();  
            ObjectOutputStream oos = new ObjectOutputStream(baos);  
            oos.writeObject(serializable);  
            oos.flush();  
            if (this.clusterMode){
            	jedisCluster.set(key.getBytes(), baos.toByteArray());            	
            }else{
            	jedis = this.jedisPool.getResource();
            	jedis.set(key.getBytes(), baos.toByteArray());
            }
    	    oos.close();
    	    baos.close();
    	    
    	    result = true;
		} catch (Throwable e) {
			System.err.println("setObject exception : " + e.getLocalizedMessage());
		} finally{
			if (!this.clusterMode){
				if (jedis != null){
					jedis.close();					
				}
			}
		}
        return result;
	}
	
	public Object getObject(String key){
		Object Object = null;
		Jedis jedis = null;
        try {
        	byte[] bytes;
        	if (this.clusterMode){
        		bytes = jedisCluster.get(key.getBytes());	
        	}else{
        		jedis = this.jedisPool.getResource();
        		bytes = jedis.get(key.getBytes());
        	}
        	 
    	    if (bytes != null){
    	    	ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        	    ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);  
        	    Object = objectInputStream.readObject();
        	    objectInputStream.close();
        	    byteArrayInputStream.close();    	    	
    	    }
		} catch (Throwable e) {
			System.err.println("getObject exception : " + e.getLocalizedMessage());
		} finally{
			if (!this.clusterMode){
				if (jedis != null){
					jedis.close();					
				}
			}
		}
        return Object;
	}
	
	public boolean removeObject(String key){
		boolean result = false;
		Jedis jedis = null;
        try {
        	if (this.clusterMode){
        		jedisCluster.del(key.getBytes());        		
        	}else{
        		jedis = this.jedisPool.getResource();
        		jedis.del(key.getBytes());
        	}
    	    result = true;
		} catch (Throwable e) {
			System.err.println("removeObject exception : " + e.getLocalizedMessage());
		} finally{
			if (!this.clusterMode){
				if (jedis != null){
					jedis.close();					
				}
			}
		}
        return result;	        	
	}
	
	public boolean existsObject (String key){
		Jedis jedis = null;
		try {
			if (this.clusterMode){
				return jedisCluster.exists(key.getBytes());			
			}else{
				jedis = this.jedisPool.getResource();
				return jedis.exists(key.getBytes());
			}
		} catch (Throwable e) {
			System.err.println("existsObject exception : " + e.getLocalizedMessage());
			return false;
		} finally{
			if (!this.clusterMode){
				if (jedis != null){
					jedis.close();					
				}
			}
		}
	}
	
	public void addSet(String key,String member){
		Jedis jedis = null;
		try {
			if (this.clusterMode){
				jedisCluster.sadd(key, member);			
			}else{
				jedis = this.jedisPool.getResource();
				jedis.sadd(key, member);
			}
		} catch (Throwable e) {
			System.err.println("addSet exception : " + e.getLocalizedMessage());
		} finally{
			if (!this.clusterMode){
				if (jedis != null){
					jedis.close();					
				}
			}
		}
	}
	
	public void removeSet(String key,String member){
		Jedis jedis = null;
		try {
			if (this.clusterMode){
				jedisCluster.srem(key, member);			
			}else{
				jedis = this.jedisPool.getResource();
				jedis.srem(key, member);
			}
		} catch (Throwable e) {
			System.err.println("removeSet exception : " + e.getLocalizedMessage());
		} finally{
			if (!this.clusterMode){
				if (jedis != null){
					jedis.close();					
				}
			}
		}
	}
	
	public Set<String> getSet(String key){
		Jedis jedis = null;
		try {
			if (this.clusterMode){
				return jedisCluster.smembers(key);			
			}else{
				jedis = this.jedisPool.getResource();
				return jedis.smembers(key);
			}
		} catch (Throwable e) {
			System.err.println("getSet exception : " + e.getLocalizedMessage());
			return null;
		} finally{
			if (!this.clusterMode){
				if (jedis != null){
					jedis.close();					
				}
			}
		}
	}
	
	public boolean isInSet(String key,String member){
		Jedis jedis = null;
		try {
			if (this.clusterMode){
				return jedisCluster.sismember(key, member);			
			}else{
				jedis = this.jedisPool.getResource();
				return jedis.sismember(key, member);
			}
		} catch (Throwable e) {
			System.err.println("isInSet exception : " + e.getLocalizedMessage());
			return false;
		} finally{
			if (!this.clusterMode){
				if (jedis != null){
					jedis.close();					
				}
			}
		}
	}
	
	public void publish(String channel,String message) {
		Jedis jedis = null;
		try {
			if (this.clusterMode){
				jedisCluster.publish(channel, message);			
			}else{
				jedis = this.jedisPool.getResource();
				jedis.publish(channel, message);
			}
		} catch (Throwable e) {
			System.err.println("isInSet exception : " + e.getLocalizedMessage());
		} finally{
			if (!this.clusterMode){
				if (jedis != null){
					jedis.close();					
				}
			}
		}		
	}
	
	public boolean setSubscribeTime(String channel,int seconds){
        boolean result = false;
        Jedis jedis = null;
        try {
        	long currentTime = System.currentTimeMillis();
        	if (this.clusterMode){
            	if (!jedisCluster.exists(channel)) {
            		jedisCluster.setex(channel,seconds,String.valueOf(currentTime));            		
            	}
            }else{
            	jedis = this.jedisPool.getResource();
            	if (!jedis.exists(channel)) {
            		jedis.setex(channel,seconds,String.valueOf(currentTime));            		
            	}
            }
    	    result = true;
		} catch (Throwable e) {
			System.err.println("setSubscribeTimer exception : " + e.getLocalizedMessage());
		} finally{
			if (!this.clusterMode){
				if (jedis != null){
					jedis.close();					
				}
			}
		}
        return result;
	}	
	
	public long getSubscribeTime(String channel){
        long result = -1;
        Jedis jedis = null;
        try {
        	if (this.clusterMode){
        		String temp = jedisCluster.get(channel);
        		if (StringUtil.isNotNullNotEmpty(temp)) {
        			result = Long.parseLong(temp);
        		}
            }else{
            	jedis = this.jedisPool.getResource();
            	String temp = jedis.get(channel);
            	result = Long.parseLong(temp);
            }
		} catch (Throwable e) {
			System.err.println("setSubscribeTimer exception : " + e.getLocalizedMessage());
		} finally{
			if (!this.clusterMode){
				if (jedis != null){
					jedis.close();					
				}
			}
		}
        return result;
	}
	
	public void subscribe(String channel,JedisPubSub jedisPubSub) {
		Jedis jedis = null;
		try {
			if (this.clusterMode){
				jedisCluster.subscribe(jedisPubSub, channel);
			}else{
				jedis = this.jedisPool.getResource();
				jedis.subscribe(jedisPubSub,channel);
			}
		} catch (Throwable e) {
			System.err.println("isInSet exception : " + e.getLocalizedMessage());
		} finally{
			if (!this.clusterMode){
				if (jedis != null){
					jedis.close();					
				}
			}
		}		
	}	
}