package com.agileai.hotweb.common;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;

import org.apache.http.Consts;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.agileai.util.IOUtil;

public class HttpClientHelper {
	protected static Logger log = Logger.getLogger(HttpClientHelper.class);
	private boolean isSSL = false;
	private String charset = "utf-8";
	protected RequestConfig requestConfig = null;
	
	public HttpClientHelper(){
		this.isSSL = false;
		this.initRequestConfig();
	}
	
	public HttpClientHelper(boolean isSSL){
		this.isSSL = isSSL;
		this.initRequestConfig();
	}
	
	public void setRequestConfig(RequestConfig requestConfig) {
		this.requestConfig = requestConfig;
	}
	
	protected void initRequestConfig(){
		this.requestConfig = RequestConfig.custom()
				.setSocketTimeout(10000)
	            .setConnectTimeout(10000)
	            .setConnectionRequestTimeout(10000)
//	            .setProxy(new HttpHost("myotherproxy", 8080))
	            .build();		
	}	
	
	public void setCharset(String charset) {
		this.charset = charset;
	}
	
	protected CloseableHttpClient getSSLHttpClient() {
		try {
			SSLContext sslContext = new SSLContextBuilder().loadTrustMaterial(
					null, new TrustStrategy() {
						// 信任所有
						public boolean isTrusted(X509Certificate[] chain,
								String authType) throws CertificateException {
							return true;
						}
					}).build();
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext);
			return HttpClients.custom().setSSLSocketFactory(sslsf).build();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		}
		return HttpClients.createDefault();
	}	
	
	public String retrieveGetReponseText(String url){
		String result = this.retrieveGetReponseText(url, new ArrayList<Header>());
		return result;
	}

	public String retrieveGetReponseText(String url,List<Header> headers){
		String result = null;
		CloseableHttpClient httpClient = null;
    	CloseableHttpResponse response = null;
		try {
			try{
				if (isSSL || url.startsWith("https://")){
					httpClient = this.getSSLHttpClient();
				}else{
					httpClient = HttpClients.createDefault();					
				}
		        HttpGet httpGet = new HttpGet(url);   
		    	if (this.requestConfig != null){
		    		httpGet.setConfig(this.requestConfig);
		    	}
		    	for (int i=0;i < headers.size();i++){
		    		Header header = headers.get(i);
		    		httpGet.addHeader(header);
		    	}
		        response = httpClient.execute(httpGet);    
		        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {  
		        	result = EntityUtils.toString(response.getEntity(),charset);
		        }
			}finally{
	    		if (response != null){
	    			response.close();
	    		}
	    		if (httpClient != null){
	    			httpClient.close();
	    		}
			}		
		}
		catch(Exception ex){
			log.error(ex.getLocalizedMessage(), ex);
		}
		return result;
	}
	
	public String retrievePostReponseText(String url,List<Header> headers,HttpEntity httpEntity){
		String result = null;
		CloseableHttpClient httpClient = null;
    	CloseableHttpResponse response = null;
		try {
			try{
				if (isSSL || url.startsWith("https://")){
					httpClient = this.getSSLHttpClient();
				}else{
					httpClient = HttpClients.createDefault();					
				}
				
		    	HttpPost httpPost = new HttpPost(url);
		    	if (this.requestConfig != null){
		    		httpPost.setConfig(this.requestConfig);
		    	}
		    	for (int i=0;i < headers.size();i++){
		    		Header header = headers.get(i);
		    		httpPost.addHeader(header);
		    	}
		        httpPost.setEntity(httpEntity);    
		        
		        response = httpClient.execute(httpPost);    
		        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {  
		        	result = EntityUtils.toString(response.getEntity(),charset);		        	
		        }
			}finally{
	    		if (response != null){
	    			response.close();
	    		}
	    		if (httpClient != null){
	    			httpClient.close();
	    		}
			}		
		}
		catch(Exception ex){
			log.error(ex.getLocalizedMessage(), ex);
		}
		return result;
	}	
	
	public String retrievePostReponseText(String url,HttpEntity httpEntity){
		String result = this.retrievePostReponseText(url,new ArrayList<Header>(),httpEntity);
		return result;
	}	
	
	public String retrievePostReponseText(String requestURL,List<NameValuePair> formparams){
		String responseText = this.retrievePostReponseText(requestURL,new ArrayList<Header>(), formparams);
        return responseText;
	}
	
	public String retrievePostReponseText(String requestURL,List<Header> headers,List<NameValuePair> formparams){
		String responseText = null;
		CloseableHttpClient httpClient  = null;
		CloseableHttpResponse response = null;
		try {
			try{
				if (isSSL || requestURL.startsWith("https://")){
					httpClient = this.getSSLHttpClient();
				}else{
					httpClient = HttpClients.createDefault();					
				}
		    	HttpPost httpPost = new HttpPost(requestURL);
		    	if (this.requestConfig != null){
		    		httpPost.setConfig(this.requestConfig);
		    	}
		    	for (int i=0;i < headers.size();i++){
		    		Header header = headers.get(i);
		    		httpPost.addHeader(header);
		    	}
				HttpEntity httpEntity = new UrlEncodedFormEntity(formparams, Consts.UTF_8);
		        httpPost.setEntity(httpEntity);
		        response = httpClient.execute(httpPost);
		        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					HttpEntity rspEntity = response.getEntity();
					InputStream inputStream = rspEntity.getContent();
					ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
					IOUtil.copyAndCloseInput(inputStream, byteArrayOutputStream);
					responseText = byteArrayOutputStream.toString("UTF-8");
		        }
			}
	        finally{
	    		if (response != null){
	    			response.close();
	    		}
	    		if (httpClient != null){
	    			httpClient.close();
	    		}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
        return responseText;
	}	
}