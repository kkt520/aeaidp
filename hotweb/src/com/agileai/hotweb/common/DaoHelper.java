package com.agileai.hotweb.common;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.orm.ibatis.SqlMapClientCallback;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.agileai.domain.DataBag;
import com.agileai.domain.DataBox;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.domain.DataSet;
import com.ibatis.sqlmap.client.SqlMapExecutor;

public class DaoHelper extends SqlMapClientDaoSupport{
	public DaoHelper(){
		super();
	}
	
	public void batchExecute(SqlMapClientCallback callback) throws DataAccessException{
		this.getSqlMapClientTemplate().execute(callback);
	}
	
	public void batchDelete(final String statementId,final List<DataParam> paramList) throws DataAccessException{
		SqlMapClientCallback callback = new SqlMapClientCallback(){
			public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
				executor.startBatch();
				for (int i=0;i < paramList.size();i++){
					DataParam param = paramList.get(i);
					executor.delete(statementId, param);
				}
				executor.executeBatch();
				return null;
			}
		};
		this.getSqlMapClientTemplate().execute(callback);
	}
	public void batchUpdate(final String statementId,final List<DataParam> paramList) throws DataAccessException{
		SqlMapClientCallback callback = new SqlMapClientCallback(){
			public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
				executor.startBatch();
				for (int i=0;i < paramList.size();i++){
					DataParam dataRow = paramList.get(i);
					executor.update(statementId, dataRow);
				}
				executor.executeBatch();
				return null;
			}
		};
		this.getSqlMapClientTemplate().execute(callback);
	}
	public void batchInsert(final String statementId,final List<DataParam> paramList) throws DataAccessException{
		SqlMapClientCallback callback = new SqlMapClientCallback(){
			public Object doInSqlMapClient(SqlMapExecutor executor) throws SQLException {
				executor.startBatch();
				for (int i=0;i < paramList.size();i++){
					DataParam dataRow = paramList.get(i);
					executor.insert(statementId, dataRow);
				}
				executor.executeBatch();
				return null;
			}
		};
		this.getSqlMapClientTemplate().execute(callback);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<DataRow> queryRecords(String statementId,DataParam param) throws DataAccessException {
		List result = new ArrayList();
		result = (List)this.getSqlMapClientTemplate().queryForList(statementId,param);
		return result;
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<DataRow> queryRecords(String statementId,Object id) throws DataAccessException {
		List result = new ArrayList();
		result = (List)this.getSqlMapClientTemplate().queryForList(statementId,id);
		return result;
	}
	@SuppressWarnings("unchecked")
	public HashMap<String,DataRow> queryRecords(String indexFieldName,String statementId,DataParam param) throws DataAccessException {
		HashMap<String,DataRow> result = new HashMap<String,DataRow>();
		List<DataRow> records = this.getSqlMapClientTemplate().queryForList(statementId,param);
		if (records != null && records.size() > 0){
			int count = records.size();
			for (int i=0;i < count;i++){
				DataRow row = records.get(i);
				result.put(String.valueOf(row.get(indexFieldName)), row);
			}
		}
		return result;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public DataSet queryDataSet(String statementId,DataParam param) throws DataAccessException {
		DataSet result = new DataSet();
		List records = (List)this.getSqlMapClientTemplate().queryForList(statementId,param);
		result.setRecords(records);
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public DataBag queryDataBag(String indexFieldName,String statementId,DataParam param) throws DataAccessException {
		DataBag result = new DataBag();
		List<DataRow> records = this.getSqlMapClientTemplate().queryForList(statementId,param);
		if (records != null && records.size() > 0){
			int count = records.size();
			for (int i=0;i < count;i++){
				DataRow row = records.get(i);
				result.getDataRows().put(String.valueOf(row.get(indexFieldName)), row);
			}
		}
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public DataBox queryDataBox(String indexFieldName,String statementId,DataParam param) throws DataAccessException {
		DataBox result = new DataBox();
		List<DataRow> records = this.getSqlMapClientTemplate().queryForList(statementId,param);
		if (records != null && records.size() > 0){
			int count = records.size();
			for (int i=0;i < count;i++){
				DataRow row = records.get(i);
				String key = String.valueOf(row.get(indexFieldName));
				DataSet dataSet = this.getDataSet(result, key);
				dataSet.addDataRow(row);
			}
		}
		return result;
	}
	
	private DataSet getDataSet(DataBox dataBox,String key){
		if (!dataBox.getDataSets().containsKey(key)){
			DataSet dataSet = new DataSet();
			dataBox.addDataSet(key, dataSet);
		}
		return dataBox.getDataSet(key);
	}
	
	public DataRow getRecord(String statementId,DataParam param) throws DataAccessException {
		DataRow result = (DataRow)this.getSqlMapClientTemplate().queryForObject(statementId, param);
		return result;
	}
	public DataRow getRecord(String statementId,Object id) throws DataAccessException {
		DataRow result = (DataRow)this.getSqlMapClientTemplate().queryForObject(statementId, id);
		return result;
	}
	public void insertRecord(String statementId,DataParam param) throws DataAccessException {
		this.getSqlMapClientTemplate().insert(statementId, param);
	}
	public void updateRecord(String statementId,DataParam param) throws DataAccessException {
		this.getSqlMapClientTemplate().update(statementId, param);
	}
	public void deleteRecords(String statementId,Object id) throws DataAccessException {
		this.getSqlMapClientTemplate().delete(statementId, id);
	}
	public void deleteRecords(String statementId,DataParam param) throws DataAccessException {
		this.getSqlMapClientTemplate().delete(statementId, param);
	}
}
