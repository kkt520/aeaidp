package com.agileai.hotweb.common;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;

import org.apache.commons.dbcp.BasicDataSource;

public class HotServerBasicDataSource extends BasicDataSource {
    public synchronized void close() throws SQLException {
        DriverManager.deregisterDriver(DriverManager.getDriver(url));
        super.close();
    }
    @SuppressWarnings("rawtypes")
	public void setConnectionProperties(Properties connectionProperties){
		for (Enumeration names = connectionProperties.propertyNames(); names
				.hasMoreElements();) {
			String name = (String) names.nextElement();
			String value = connectionProperties.getProperty(name);
			addConnectionProperty(name, value);
		}
    }
}