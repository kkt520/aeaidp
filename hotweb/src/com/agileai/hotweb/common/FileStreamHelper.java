package com.agileai.hotweb.common;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLEncoder;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;

import com.agileai.util.IOUtil;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.ObjectWrapper;
import freemarker.template.Template;

public class FileStreamHelper {
	protected HttpServletRequest request = null;
	protected HttpServletResponse response = null;
	
	public FileStreamHelper(HttpServletRequest request,HttpServletResponse response){
		this.request = request;
		this.response = response;
	}
	
	/**
	 * @param templateDir   eg: /opt/resources/templates or d:/resources/templates 
	 * @param templateFile  eg: /DayReport.ftl
	 * @param exportFileName
	 * @param modelMap
	 */
	@SuppressWarnings("rawtypes")
	public void exportFile(String templateDir,String templateFile,String exportFileName,Map modelMap) {
		exportFile(templateDir,templateFile,exportFileName,modelMap, "utf-8");
	}	
	
	/**
	 * @param templateDir   eg: /opt/resources/templates or d:/resources/templates 
	 * @param templateFile  eg: /DayReport.ftl
	 * @param exportFileName
	 * @param modelMap
	 * @param charencoding  eg: utf-8
	 */
	@SuppressWarnings("rawtypes")
	public void exportFile(String templateDir,String templateFile,String exportFileName,Map modelMap,String charencoding) {
		try {
			Configuration cfg = new Configuration();
			cfg.setDirectoryForTemplateLoading(new File(templateDir));
			cfg.setObjectWrapper(new DefaultObjectWrapper());
			cfg.setEncoding(Locale.getDefault(), charencoding);
			Template temp = cfg.getTemplate(templateFile);
			temp.setEncoding(charencoding);
			
			setReponse(exportFileName);
			
        	OutputStream outputStream = response.getOutputStream();
			Writer out = new BufferedWriter(new OutputStreamWriter(outputStream, charencoding));
			temp.process(modelMap, out);
			out.flush();
			IOUtils.closeQuietly(outputStream);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("rawtypes")
	public void exportFile(String templateText,Map modelMap,String exportFileName){
		exportFile(templateText, modelMap,exportFileName,"utf-8");
	}
	
	@SuppressWarnings("rawtypes")
	public void exportFile(String templateText,Map modelMap,String exportFileName,String charencoding){
		try {
			StringWriter writer = new StringWriter();
        	Configuration cfg = new Configuration();
        	cfg.setTemplateLoader(new StringTemplateLoader(templateText));  
        	cfg.setEncoding(Locale.getDefault(), charencoding);
            cfg.setObjectWrapper(ObjectWrapper.BEANS_WRAPPER);
        	Template temp = cfg.getTemplate("");
        	temp.setEncoding(charencoding);
        	
        	this.setReponse(exportFileName);
        	
        	OutputStream outputStream = response.getOutputStream();
			Writer out = new BufferedWriter(new OutputStreamWriter(outputStream,charencoding));
			temp.process(modelMap, out);
			
            writer.flush();
            IOUtils.closeQuietly(outputStream);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void downloadFile(InputStream inputStream,String downloadFileName)  {
        try {
        	this.setReponse(downloadFileName);
        	OutputStream outputStream = response.getOutputStream();
        	IOUtil.copy(inputStream, outputStream);
        	IOUtils.closeQuietly(inputStream);
        	IOUtils.closeQuietly(outputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	private void setReponse(String fileName){
    	String agent = request.getHeader("USER-AGENT");
        try {
        	fileName= encodeFileName(fileName,agent);
   	    } catch (UnsupportedEncodingException e) {
   	    	e.printStackTrace();
   		}
        response.setHeader("Content-Disposition", "attachment;filename=\"" + fileName + "\"");
        response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
        response.setHeader("Pragma", "public");
        response.setDateHeader("Expires", (System.currentTimeMillis() + 1000));
	}
	
	private String encodeFileName(String fileName, String agent) throws UnsupportedEncodingException{
		String codedfilename = null;
		fileName=fileName.replaceAll("\n|\r", " ").trim();
		if (null != agent && (-1 != agent.indexOf("MSIE") || -1 != agent.indexOf("Trident"))) {
			codedfilename = URLEncoder.encode(fileName, "UTF8");
		} else if (null != agent && -1 != agent.indexOf("Mozilla")) {
			codedfilename = "=?UTF-8?B?"+(new String(Base64.encodeBase64(fileName.getBytes("UTF-8"))))+"?=";
		} else {
			codedfilename = fileName;
		}
		return codedfilename;
	}	
}