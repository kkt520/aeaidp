package com.agileai.hotweb.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class TreeModel implements Serializable{
	private static final long serialVersionUID = -7480834278139411483L;
	
	private String id = null;
	private String name = null;
	private String type = null;
	private TreeModel parent = null;
	private List<TreeModel> children = new ArrayList<TreeModel>();
	private Map<String,TreeModel> childrenContainer = new HashMap<String,TreeModel>();
	@SuppressWarnings("rawtypes")
	private HashMap property = new HashMap();	
	private List<String> descendantIds = new ArrayList<String>();
	private Map<String,TreeModel> fullTreeMap = new HashMap<String,TreeModel>();
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public TreeModel getParent() {
		return parent;
	}
	public void setParent(TreeModel parent) {
		this.parent = parent;
	}
	public List<TreeModel> getChildren() {
		return children;
	}
	public void addChild(TreeModel treeModel){
		this.children.add(treeModel);
		this.childrenContainer.put(treeModel.getId(), treeModel);
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Map<String,TreeModel> getChildrenMap(){
		return this.childrenContainer;
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void processRow(HashMap row){
		Iterator iter = row.keySet().iterator();
		while (iter.hasNext()){
			Object key = iter.next();
			Object value = row.get(key);
			this.property.put(key, value);
		}
	}
	@SuppressWarnings("rawtypes")
	public HashMap getProperty() {
		return property;
	}
	public List<String> getDescendantIds() {
		return descendantIds;
	}
	public Map<String, TreeModel> getFullTreeMap() {
		return fullTreeMap;
	}
}