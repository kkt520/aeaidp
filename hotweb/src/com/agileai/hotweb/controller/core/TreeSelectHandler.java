package com.agileai.hotweb.controller.core;

import java.util.ArrayList;
import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.bizmoduler.core.TreeSelectService;
import com.agileai.hotweb.domain.TreeBuilder;
import com.agileai.hotweb.domain.TreeModel;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.ListUtil;

public abstract class TreeSelectHandler extends BaseHandler{
	protected static final String newRow = "\r\n";
	protected boolean isMuilSelect = false;
	protected List<String> mulSelectNodeTypes = new ArrayList<String>();
	protected List<String> invisiableCheckBoxIdList = new ArrayList<String>(); 
	protected boolean checkRelParentNode = true;
	
	public ViewRenderer prepareDisplay(DataParam param){
		this.setAttributes(param);
		TreeBuilder treeBuilder = provideTreeBuilder(param);
		TreeModel topTreeModel = treeBuilder.buildTreeModel();
		String pickTreeSyntax = null;
		if (isMuilSelect){
			pickTreeSyntax = getMuliPickTreeSyntax(topTreeModel, new StringBuffer());
		}else{
			pickTreeSyntax = getTreeSyntax(topTreeModel,new StringBuffer());	
		}
		this.setAttribute("pickTreeSyntax", pickTreeSyntax);
		return new LocalRenderer(getPage());
	}	
	protected String getTreeSyntax(TreeModel treeModel,StringBuffer treeSyntax){
    	String result = null;
    	try {
    		treeSyntax.append("d = new dTree('d');").append(newRow);
            String rootId = treeModel.getId();
            String rootName = treeModel.getName();
            treeSyntax.append("d.add('"+rootId+"',-1,'"+rootName+"',\"javascript:setSelectTempValue('"+rootId+"','"+rootName+"')\");").append(newRow);
            buildTreeSyntax(treeSyntax,treeModel);
            treeSyntax.append("document.write(d);");
    		result = treeSyntax.toString();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
    }
	protected void buildTreeSyntax(StringBuffer treeSyntax,TreeModel treeModel){
		List<TreeModel> children = treeModel.getChildren();
		String parentId = treeModel.getId();
        for (int i=0;i < children.size();i++){
        	TreeModel child = children.get(i);
            String curNodeId = child.getId();
            String curNodeName = child.getName();
            if (!ListUtil.isNullOrEmpty(child.getChildren())){
            	treeSyntax.append("d.add('"+curNodeId+"','"+parentId+"','"+curNodeName+"',\"javascript:setSelectTempValue('"+curNodeId+"','"+curNodeName+"')\");").append(newRow);
            }else{
            	treeSyntax.append("d.add('"+curNodeId+"','"+parentId+"','"+curNodeName+"',\"javascript:setSelectTempValue('"+curNodeId+"','"+curNodeName+"')\");").append(newRow);
            }
            this.buildTreeSyntax(treeSyntax,child);
        }
    }    
	
	protected String getMuliPickTreeSyntax(TreeModel topTreeModel,StringBuffer treeSyntax){
    	String result = null;
    	try {
    		treeSyntax.append("tree = new sTree();").append(newRow);
    		treeSyntax.append("tree.setImagePath('images/stree/');").append(newRow);
    		treeSyntax.append("tree.checkRelParentNode = ").append(String.valueOf(checkRelParentNode)).append(";").append(newRow);
            String rootId = topTreeModel.getId();
            String rootName = topTreeModel.getName();
            String nodeType = topTreeModel.getType();
            boolean hasCheckBox = !invisiableCheckBoxIdList.contains(rootId);
            
            if (mulSelectNodeTypes.isEmpty()){
            	treeSyntax.append("tree.add('"+rootId+"','0','"+rootName+"','javascript:void(0)',Icon.root.src,"+String.valueOf(hasCheckBox)+");").append(newRow);            	
            }else{
            	if (mulSelectNodeTypes.contains(nodeType)){
            		treeSyntax.append("tree.add('"+rootId+"','0','"+rootName+"','javascript:void(0)',Icon.root.src,"+String.valueOf(hasCheckBox)+");").append(newRow);	
            	}else{
            		treeSyntax.append("tree.add('"+rootId+"','0','"+rootName+"','javascript:void(0)',Icon.root.src,false);").append(newRow);            		
            	}
            }
            buildMultiPickTreeSyntax(topTreeModel,treeSyntax);
            treeSyntax.append("tree.setName='selectTree';").append(newRow);
            treeSyntax.append("tree.drawTree('selectTreeContainer');");
    		result = treeSyntax.toString();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
    }
	protected void buildMultiPickTreeSyntax(TreeModel treeModel,StringBuffer treeSyntax){
    	List<TreeModel> arrayList = treeModel.getChildren();
    	String pId = treeModel.getId();
        for (int i=0;i < arrayList.size();i++){
        	TreeModel curTreeModel = arrayList.get(i);
            String curId = curTreeModel.getId();
            String curName = curTreeModel.getName();
            String curType = curTreeModel.getType();
            boolean hasCheckBox = !invisiableCheckBoxIdList.contains(curId);
            if (mulSelectNodeTypes.isEmpty()){
            	treeSyntax.append("tree.add('"+curId+"','"+pId+"','"+curName+"','javascript:void(0)',null,"+String.valueOf(hasCheckBox)+");").append(newRow);
            }else{
                if (mulSelectNodeTypes.contains(curType)){
                	treeSyntax.append("tree.add('"+curId+"','"+pId+"','"+curName+"','javascript:void(0)',null,"+String.valueOf(hasCheckBox)+");").append(newRow);
                }else{
                	treeSyntax.append("tree.add('"+curId+"','"+pId+"','"+curName+"','javascript:void(0)',null,false);").append(newRow);
                }            	
            }
            this.buildMultiPickTreeSyntax(curTreeModel,treeSyntax);
        }
    }    
	abstract protected TreeBuilder provideTreeBuilder(DataParam param);
	
	protected TreeSelectService getService() {
		return (TreeSelectService)this.lookupService(this.getServiceId());
	}	
}
