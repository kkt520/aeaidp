package com.agileai.hotweb.controller.core;

import java.util.Enumeration;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class SimpleHandler extends BaseHandler{
	public SimpleHandler(){
		super();
	}
	public ViewRenderer prepareDisplay(DataParam param) {
		this.setAttributes(param);
		Enumeration<String> enumer = request.getAttributeNames();
		while(enumer.hasMoreElements()){
			String key = enumer.nextElement();
			Object value = request.getAttribute(key);
			this.setAttribute(key, value);
		}
		this.processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
}
