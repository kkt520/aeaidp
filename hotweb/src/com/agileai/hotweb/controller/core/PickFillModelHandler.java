package com.agileai.hotweb.controller.core;

import java.util.List;

import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.PickFillModelService;
import com.agileai.hotweb.renders.LocalRenderer;
import com.agileai.hotweb.renders.ViewRenderer;

public class PickFillModelHandler extends BaseHandler{
	public PickFillModelHandler(){
		super();
	}
	public ViewRenderer prepareDisplay(DataParam param){
		mergeParam(param);
		initParameters(param);
		this.setAttributes(param);
		List<DataRow> rsList = getService().queryPickFillRecords(param);
		this.setRsList(rsList);
		processPageAttributes(param);
		return new LocalRenderer(getPage());
	}
	
	public ViewRenderer doQueryAction(DataParam param){
		return prepareDisplay(param);
	}

	protected PickFillModelService getService() {
		return (PickFillModelService)this.lookupService(this.getServiceId());
	}
}