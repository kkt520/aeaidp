package com.agileai.hotweb.cache;

import java.io.Serializable;
import java.util.Set;

import com.agileai.hotweb.common.RedisHelper;

import redis.clients.jedis.JedisPoolConfig;

public class RedisCacheServiceImpl implements CacheService{
	private String appName = "default";
	private RedisHelper redisHelper = null;
	
	public RedisCacheServiceImpl(JedisPoolConfig jedisPoolConfig,String hosts,String appName){
		this.redisHelper = RedisHelper.getOnly(jedisPoolConfig, hosts);
		this.appName = appName;
	}
	
	@Override
	public void putValue(String key, Serializable serializable) {
		this.redisHelper.setObject(appName+"-"+key, serializable);
	}

	@Override
	public Object getValue(String key) {
		return this.redisHelper.getObject(appName+"-"+key);
	}

	@Override
	public boolean existsValue(String key) {
		return this.redisHelper.existsObject(appName+"-"+key);
	}

	@Override
	public void removeValue(String key) {
		this.redisHelper.removeObject(appName+"-"+key);
	}

	@Override
	public boolean isDistributed() {
		return true;
	}

	@Override
	public void addSet(String setKey, String member) {
		this.redisHelper.addSet(appName+"-"+setKey, member);
	}

	@Override
	public void removeSet(String setKey, String member) {
		this.redisHelper.removeSet(appName+"-"+setKey, member);
	}

	@Override
	public Set<String> getSet(String setKey) {
		return this.redisHelper.getSet(appName+"-"+setKey);
	}

	@Override
	public boolean isInSet(String setKey, String member) {
		return this.redisHelper.isInSet(appName+"-"+setKey, member);
	}
}
