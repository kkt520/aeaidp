package com.agileai.hotweb.module.system.service;

import java.util.List;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.bizmoduler.core.TreeManage;

public interface SecurityRoleTreeManage
        extends TreeManage {
	public List<DataRow> queryAuthRecords(String roleId);
}
