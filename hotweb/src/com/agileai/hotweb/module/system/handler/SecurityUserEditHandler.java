package com.agileai.hotweb.module.system.handler;

import com.agileai.common.KeyGenerator;
import com.agileai.domain.DataParam;
import com.agileai.hotweb.bizmoduler.core.TreeAndContentManage;
import com.agileai.hotweb.controller.core.TreeAndContentManageEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.hotweb.module.system.service.SecurityUserManage;
import com.agileai.util.CryptionUtil;

public class SecurityUserEditHandler
        extends TreeAndContentManageEditHandler {
    public SecurityUserEditHandler() {
        super();
        this.serviceId = buildServiceId(SecurityUserManage.class);
        this.tabId = "SecurityUser";
        this.columnIdField = "GRP_ID";
        this.contentIdField = "USER_ID";
    }

    protected void processPageAttributes(DataParam param) {
        setAttribute("USER_SEX",
                     FormSelectFactory.create("USER_SEX")
                                      .addSelectedValue(getOperaAttributeValue("USER_SEX",
                                                                               "M")));
        setAttribute("USER_STATE",
                     FormSelectFactory.create("SYS_VALID_TYPE")
                                      .addSelectedValue(getOperaAttributeValue("USER_STATE",
                                                                               "1")));
    }

    protected SecurityUserManage getService() {
        return (SecurityUserManage) this.lookupService(this.getServiceId());
    }
    
    @Override
	public ViewRenderer doSaveAction(DataParam param){
		String rspText = SUCCESS;
		
		TreeAndContentManage service = this.getService();
		String operateType = param.get(OperaType.KEY);
		String userId = KeyGenerator.instance().genKey();
		String guId = KeyGenerator.instance().genKey();

		
		if (OperaType.CREATE.equals(operateType)){
			String colIdField = service.getTabIdAndColFieldMapping().get(this.tabId);
			String userPwd = param.get("USER_PWD");
			param.put("USER_PWD",CryptionUtil.md5Hex(userPwd));
			param.put("USER_ID",userId);
			param.put("GU_ID",guId);
			String columnId = param.get(this.columnIdField);
			param.put(colIdField,columnId);
			getService().createtContentRecord(tabId,param);	
		}
		else if (OperaType.UPDATE.equals(operateType)){
			getService().updatetContentRecord(tabId,param);	
		}
		
		return new AjaxRenderer(rspText);
	}
}
